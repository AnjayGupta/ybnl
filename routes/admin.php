<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\AppliedJobController;
use App\Http\Controllers\Admin\EventsController;
use App\Http\Controllers\Admin\JobController;
use App\Http\Controllers\Admin\MailController;
use App\Http\Controllers\Admin\ProgrammersController;
use App\Http\Controllers\Admin\ResourceController;
use App\Http\Controllers\Admin\SettingController;

/*
|--------------------------------------------------------------------------
Admin Routes Coding sart here --------------------------------------------------------------------------
|
| Here is where you can register admin  routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::group(['prefix'=>"admin","middleware"=>"auth"],function(){
//     Route::get("/dashboard",[AdminController::class,'index'])->name("admin.dashboard");

//     //--------------------------------- event routing start-------------------------------------//
//     Route::get("/event",[EventsController::class,'index'])->name("admin.event.index");
//     Route::get("/event",[EventsController::class,'index'])->name("admin.events");
//     //--------------------------------- end event routing end here -----------------------------//


//     //----------------------  PROGRAMMERS ROUTING START HERE -----------------------------------//
//     Route::get("/programmers",[ProgrammersController::class,'index'])->name("admin.programmers");
//     //---------------------- END PROGRAMMERS ROUTING END HERE ----------------------------------//


//      //----------------------  JOBS ROUTING START HERE -----------------------------------//
//      Route::get("/programmers",[JobController::class,'index'])->name("job.index");
//      //---------------------- END JOBS ROUTING END HERE ----------------------------------//

// });




