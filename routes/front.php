<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Front\ProgrammesController;
use App\Http\Controllers\Front\FrontController;
use App\Http\Controllers\Front\JobSeekerController;
use App\Http\Controllers\Front\EntrepreneurController;
use App\Http\Controllers\Controller;
Route::get('/', [FrontController::class, 'index'])->name('home');
Route::get('/about-us', [FrontController::class, 'abouts_us'])->name('about-us');
Route::get('/programmers', [ProgrammesController::class, 'list'])->name('programmes');
Route::get('/jobs', [FrontController::class, 'jobs'])->name('jobs');
Route::get('/resources', [FrontController::class, 'resources'])->name('resources');
Route::get('/blogs', [FrontController::class, 'blogs'])->name('blogs');
Route::get('/event', [FrontController::class, 'events'])->name('events');
Route::get('/contact-us', [FrontController::class, 'contact_us'])->name('contact-us');
Route::get('/blog-details/{slug}', [FrontController::class, 'blog_details'])->name('blog-details');
Route::get('/job-details/{any}', [FrontController::class, 'job_details'])->name('job-details');
Route::get('/event-details/{any}', [FrontController::class, 'event_details'])->name('events.deatils');
Route::get("/join-community",[FrontController::class, 'joinCommunity'])->name("join.community");
Route::get("/programmers/{title}",[ProgrammesController::class,'programmesDetails'])->name("programmes.details");
Route::post("/join-community/store",[FrontController::class,'joinCommunityStore'])->name("join.community.store");
Route::get('/page/{slug}', [FrontController::class, 'page'])->name('page');
Route::get('/job-seekers', [JobSeekerController::class, 'index'])->name('job-seekers');
Route::get('/job-seeker/{slug}', [JobSeekerController::class, 'view'])->name('job-seeker.view');
Route::get('/entrepreneurs', [EntrepreneurController::class, 'index'])->name('entrepreneurs');
Route::get('/entrepreneur/{slug}', [EntrepreneurController::class, 'view'])->name('entrepreneur.view');
?>