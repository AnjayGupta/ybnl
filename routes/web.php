<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\ProgrammersController;
use App\Http\Controllers\Admin\JobController;
use App\Http\Controllers\Admin\EventsController;
use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\AppliedJobController;
use App\Http\Controllers\Admin\MailController;
use App\Http\Controllers\Admin\ResourceController;
use App\Http\Controllers\Admin\BlogController;
use App\Http\Controllers\Admin\CommunityController;
use App\Http\Controllers\Admin\TeamMemberController;
use App\Http\Controllers\Admin\PageController;
use App\Http\Controllers\Admin\JobSeekerController;
use App\Http\Controllers\Admin\EntrepreneurController;
use App\Http\Controllers\Admin\HomepageSettingsController;
use Illuminate\Support\Facades\Artisan;

Route::get('/clear', function () {
   Artisan::call('cache:clear');
   Artisan::call('route:clear');
   Artisan::call('view:clear');
   Artisan::call('config:clear');

   return "Cache cleared successfully";
});


Route::get("/info",function(){
echo phpinfo();
});
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
////

// Route::get('/', function () {
//     return view('pages.home');
// });

// Route::get('admin11/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth', 'verified'])->name('dashboard');

// route('login');
 // pages
 Route::get('/login', function () {
    return redirect('admin/login');
})->name('admin.login');

 Route::get('admin/login', function () {
    return view('auth.login');
})->name('admin.login');

//  return view('auth.login');


Route::group(['middleware' => ['auth', 'verified'], 'prefix' => 'admin'], function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
   
    // admin
    Route::get('/dashboard', [AdminController::class, 'index'])->name('admin.dashboard');
    Route::get('/programmers', [ProgrammersController::class, 'index'])->name('admin.programmers');
    Route::get('/add-programmer', [ProgrammersController::class, 'add_programmer'])->name('admin.add_programmer');
    Route::post('/store-programmer', [ProgrammersController::class, 'store'])->name('admin.store.programmer');
    Route::get('/edit-programer/{id}', [ProgrammersController::class, 'edit'])->name('admin.edit.programer');
    Route::post('/update-programer', [ProgrammersController::class, 'update'])->name('admin.update.programer');
    Route::get('/delete-programer/{id}', [ProgrammersController::class, 'delete'])->name('admin.delete.programer');
    Route::get('/delete-programer-gallery-image/{id}', [ProgrammersController::class, 'deleteGalleryImage'])->name('admin.delete.programer_gallery_image');

    Route::get('/jobs', [JobController::class, 'index'])->name('job.index');
    Route::get('/add-job', [JobController::class, 'add'])->name('admin.add.job');
    Route::post('/store-job', [JobController::class, 'store'])->name('admin.store.job');
    Route::get('/edit-job/{id}', [JobController::class, 'edit'])->name('admin.edit.job');
    Route::post('/update-job', [JobController::class, 'update'])->name('admin.update.job');
    Route::get('/delete-job/{id}', [JobController::class, 'delete'])->name('admin.delete.job');

    Route::get('/events', [EventsController::class, 'index'])->name('admin.event.index');
    Route::get('/add-event', [EventsController::class, 'add'])->name('admin.add.event');
    Route::post('/store-event', [EventsController::class, 'store'])->name('admin.store.event');
    Route::get('/edit-event/{id}', [EventsController::class, 'edit'])->name('admin.edit.event');
    Route::post('/update-event', [EventsController::class, 'update'])->name('admin.update.event');
    Route::get('/delete-event/{id}', [EventsController::class, 'delete'])->name('admin.delete.event');


    Route::get('/setting', [SettingController::class, 'index'])->name('admin.setting');
    Route::post("/setting/update",[SettingController::class,'update'])->name("admin.setting.update");

   Route::get("/applied-job",[AppliedJobController::class,'list'])->name("admin.applied_job");
   Route::get("/applied-job/delete",[AppliedJobController::class,'delete'])->name("admin.applied_job.delete");
   Route::get("/create-mail",[MailController::class,'create'])->name("admin.mail.create");
   Route::post("/mail/store",[MailController::class,'store'])->name("admin.mail.store");
   Route::get("/mail/delete/{id}",[MailController::class,'delete'])->name("admin.mail.delete");

   Route::get("/resouce",[ResourceController::class,'create'])->name("admin.resource.index");
   Route::get("/resouce/list",[ResourceController::class,'list'])->name("admin.resource.list");
   Route::post("/resouce/store",[ResourceController::class,'store'])->name("admin.resource.store");
   Route::post("/resouce/delete/{id}",[ResourceController::class,'delete'])->name("admin.resource.delete");
   Route::get("/resouce/edit/{id}",[ResourceController::class,'edit'])->name("admin.resource.edit");
   Route::post("/resouce/update",[ResourceController::class,'update'])->name("admin.resource.update");

   Route::post("/profile-update",[AdminController::class,'updateProfile'])->name("admin.profile.update");


   //blog  routing start here
    Route::get("/blog",[BlogController::class,'index'])->name("admin.blog.index");
    Route::post("/blog/store",[BlogController::class,'store'])->name("admin.blog.store");
    Route::get("/blog/list",[BlogController::class,'list'])->name("admin.blog.list");
    Route::get("/blog/edit/{id}",[BlogController::class,'edit'])->name("admin.blog.edit");
    Route::get("/blog/delete/{id}",[BlogController::class,'delete'])->name("admin.blog.delete");
    Route::post("/blog/update",[BlogController::class,'update'])->name("admin.blog.update");
   
   //end blog routing end here

   // community page routing start here
   Route::get("/community",[CommunityController::class,'index'])->name("admin.community.list");
   Route::get("/community/delete/{id}",[CommunityController::class,'delete'])->name("admin.community.delete");
   
   // end community page routing end here

   // team member page routing start here
   Route::get("/team-members",[TeamMemberController::class,'index'])->name("admin.team_member.index");
   Route::get("/team-member/create",[TeamMemberController::class,'create'])->name("admin.team_member.add");
   Route::post("/team-member/store",[TeamMemberController::class,'store'])->name("admin.team_member.store");
   Route::get("/team-member/edit/{id}",[TeamMemberController::class,'edit'])->name("admin.team_member.edit");
   Route::post("/team-member/update",[TeamMemberController::class,'update'])->name("admin.team_member.update");
   Route::get("/team-member/delete/{id}",[TeamMemberController::class,'delete'])->name("admin.team_member.delete");
   // team member page routing end here

   // pages page routing start here
   Route::get("/pages",[PageController::class,'index'])->name("admin.page.index");
   Route::get("/page/create",[PageController::class,'create'])->name("admin.page.add");
   Route::post("/page/store",[PageController::class,'store'])->name("admin.page.store");
   Route::get("/page/edit/{id}",[PageController::class,'edit'])->name("admin.page.edit");
   Route::post("/page/update",[PageController::class,'update'])->name("admin.page.update");
   Route::get("/page/delete/{id}",[PageController::class,'delete'])->name("admin.page.delete");
   // pages page routing end here

   // job seekers page routing start here
   Route::get("/job-seekers", [JobSeekerController::class,'index'])->name("admin.job_seeker.index");
   Route::get("/job-seeker/create", [JobSeekerController::class,'create'])->name("admin.job_seeker.add");
   Route::post("/job-seeker/store", [JobSeekerController::class,'store'])->name("admin.job_seeker.store");
   Route::get("/job-seeker/edit/{id}", [JobSeekerController::class,'edit'])->name("admin.job_seeker.edit");
   Route::post("/job-seeker/update", [JobSeekerController::class,'update'])->name("admin.job_seeker.update");
   Route::get("/job-seeker/delete/{id}", [JobSeekerController::class,'delete'])->name("admin.job_seeker.delete");
   // job seekers page routing end here

   // entrepreneurs page routing start here
   Route::get("/entrepreneurs", [EntrepreneurController::class,'index'])->name("admin.entrepreneur.index");
   Route::get("/entrepreneur/create", [EntrepreneurController::class,'create'])->name("admin.entrepreneur.add");
   Route::post("/entrepreneur/store", [EntrepreneurController::class,'store'])->name("admin.entrepreneur.store");
   Route::get("/entrepreneur/edit/{id}", [EntrepreneurController::class,'edit'])->name("admin.entrepreneur.edit");
   Route::post("/entrepreneur/update", [EntrepreneurController::class,'update'])->name("admin.entrepreneur.update");
   Route::get("/entrepreneur/delete/{id}", [EntrepreneurController::class,'delete'])->name("admin.entrepreneur.delete");
   // entrepreneurs page routing end here

   // banners page routing start here
   Route::get("/banners", [HomepageSettingsController::class, 'banners'])->name("admin.banners");
   Route::get("/banner/create", [HomepageSettingsController::class,'bannerCreate'])->name("admin.banner.add");
   Route::post("/banner/store", [HomepageSettingsController::class,'bannerStore'])->name("admin.banner.store");
   Route::get("/banner/edit/{id}", [HomepageSettingsController::class,'bannerEdit'])->name("admin.banner.edit");
   Route::post("/banner/update", [HomepageSettingsController::class,'bannerUpdate'])->name("admin.banner.update");
   Route::get("/banner/delete/{id}", [HomepageSettingsController::class,'bannerDelete'])->name("admin.banner.delete");
   // banners page routing end here

   // profiles page routing start here
   Route::get("/profiles", [HomepageSettingsController::class, 'profiles'])->name("admin.profiles");
   Route::get("/profile/create", [HomepageSettingsController::class,'profileCreate'])->name("admin.profile.add");
   Route::post("/profile/store", [HomepageSettingsController::class,'profileStore'])->name("admin.profile.store");
   Route::get("/profile/edit/{id}", [HomepageSettingsController::class,'profileEdit'])->name("admin.profile.edit");
   Route::post("/profile/update", [HomepageSettingsController::class,'profileUpdate'])->name("admin.profile.update");
   Route::get("/profile/delete/{id}", [HomepageSettingsController::class,'profileDelete'])->name("admin.profile.delete");
   // profiles page routing end here

});

//--------------------------front page routing start here ------------------------//

//-------------------------- end front page routing end here ---------------------//
Route::post("/job/apply",[AppliedJobController::class,'applied'])->name("job.applied");

require __DIR__.'/auth.php';


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');