<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('programmers', function (Blueprint $table) {
            $table->longText('eligibility_criteria')->nullable();
            $table->longText('process')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('programmers', function (Blueprint $table) {
            $table->dropColumn('eligibility_criteria');
            $table->dropColumn('process');
        });
    }
};
