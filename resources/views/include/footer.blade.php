<!-- Button trigger modal -->

  
  <!-- Modal -->
  <div class="modal" id="profile-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog  rounded-0 modal-lg" role="document">
	  <div class="modal-content rounded-0">
		<div class="modal-header">
			<h3 class="modal-title font-weight-bolder" id="exampleModalLabel">Update Profile</h3>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		<div class="modal-body rounded-0">
		  <form class="profile-update-form"  method="POST" enctype="multipart/form-data">
			@csrf
			<div class="row">
				<div class="col">
					<div class="form-group">
						<label for="">Name</label>
						<input type="text" name="name" id="" class="form-control" placeholder="Name" value="{{auth()->user()->name}}">
					</div>
				</div>

				<div class="col">
					<div class="form-group">
						<label for="">Email</label>
						<input type="text" name="email" id="" class="form-control" placeholder="Email" required value="{{auth()->user()->email}}">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<div class="form-group">
						<label for="">Phone</label>
						<input type="text" name="phone" id="" class="form-control" placeholder="Phone" value="{{auth()->user()->phone}}">
					</div>
				</div>

				<div class="col">
					<div class="form-group">
						<label for="">Profile</label>
						<input type="file" name="profile" id="" class="form-control" placeholder="">
						<img src="{{auth()->user()->profile}}" alt="" width="100">
					</div>
				</div>
			</div>
			<div class="row">
				

				<div class="col">
					<div class="form-group">
						<label for="">Address</label>
						<textarea name="address" id=""  class="form-control">
							{{auth()->user()->address}}
						</textarea>
					</div>
				</div>
			</div>

			<div class="form-group">
				<button class="btn btn-primary ">
					Update Profile
				</button>
			</div>
		  </form>
		</div>
		
	  </div>
	</div>
  </div>
<script src="{{asset('admin_assets/assets/js/core/jquery.3.2.1.min.js')}}"></script>
	<script src="{{asset('admin_assets/assets/js/core/popper.min.js')}}"></script>
	<script src="{{asset('admin_assets/assets/js/core/bootstrap.min.js')}}"></script>

	<!-- jQuery UI -->
	<script src="{{asset('admin_assets/assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js')}}"></script>
	<script src="{{asset('admin_assets/assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js')}}"></script>

	<!-- jQuery Scrollbar -->
	<script src="{{asset('admin_assets/assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js')}}"></script>


	<!-- Chart JS -->
	<script src="{{asset('admin_assets/assets/js/plugin/chart.js/chart.min.js')}}"></script>

	<!-- jQuery Sparkline -->
	<script src="{{asset('admin_assets/assets/js/plugin/jquery.sparkline/jquery.sparkline.min.js')}}"></script>

	<!-- Chart Circle -->
	<script src="{{asset('admin_assets/assets/js/plugin/chart-circle/circles.min.js')}}"></script>

	<!-- Datatables -->


	<!-- Bootstrap Notify -->
	<script src="{{asset('admin_assets/assets/js/plugin/bootstrap-notify/bootstrap-notify.min.js')}}"></script>

	<!-- jQuery Vector Maps -->
	<script src="{{asset('admin_assets/assets/js/plugin/jqvmap/jquery.vmap.min.js')}}"></script>
	<script src="{{asset('admin_assets/assets/js/plugin/jqvmap/maps/jquery.vmap.world.js')}}"></script>

	<!-- Sweet Alert -->
	<script src="{{asset('admin_assets/assets/js/plugin/sweetalert/sweetalert.min.js')}}"></script>

	<!-- Atlantis JS -->
	<script src="{{asset('admin_assets/assets/js/atlantis.min.js')}}"></script>

	<!-- Atlantis DEMO methods, don't include it in your project! -->
	<script src="{{asset('admin_assets/assets/js/setting-demo.js')}}"></script>
	<script src="{{asset('admin_assets/assets/js/demo.js')}}"></script>
	<script src="{{asset('admin_assets/js/sweet-alert.min.js')}}"></script>
	@yield("js")
	<script>
		$(".profile-update-form").submit(function(e){
			e.preventDefault();
			var formdata=new FormData(this);
			$.ajax({
              type:"POST",
			  url:"{{route('admin.profile.update')}}",
			  data:formdata,
			  processData:false,
			  contentType:false,
			  cache:false,
			  beforeSend:function(){
				$(this).find("button").attr("disabled",true);
				$(this).find("button").text("Please wait...");
			  },
			  success:function(response){
				if(response.status){
					$(this).find("button").attr("disabled",false);
				    $(this).find("button").text("Update Profile");
					const Toast = Swal.mixin({
                   toast: true,
                   position: 'top-end',
               showConfirmButton: false,
              timer: 3000,
           timerProgressBar: true,
             didOpen: (toast) => {
             toast.addEventListener('mouseenter', Swal.stopTimer)
         toast.addEventListener('mouseleave', Swal.resumeTimer)
         }
})

Toast.fire({
  icon: 'success',
  title: 'Profile update succssfully'
})
				    //Swal.fire('Profile updated successfully', '', 'success');
				    setTimeout(() => {
					  window.location.reload();
				      }, 1000);
				      }
			  }
			});
		});
	</script>

	
</body>
</html>