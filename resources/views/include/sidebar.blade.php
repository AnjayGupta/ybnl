<div class="sidebar sidebar-style-2">			
			<div class="sidebar-wrapper scrollbar scrollbar-inner">
				<div class="sidebar-content">
					<div class="user">
						<div class="avatar-sm float-left mr-2">
							@if(auth()->user()->profile=="")
							<img src="{{asset('admin_assets/assets/img/profile.jpg')}}" alt="..." class="avatar-img rounded-circle">
							@else
							<img src="{{auth()->user()->profile}}" alt="..." class="avatar-img rounded-circle">
							@endif
						</div>
						<div class="info">
							<a  href="#collapseExample" aria-expanded="true">
								<span>
									
									<span class="user-level">{{auth()->user()->name}}</span>
									
								</span>
							</a>
							<div class="clearfix"></div>

							
						</div>
					</div>
					<ul class="nav nav-primary">
						<li class="nav-item mb-2 {{activeMenu('admin/dashboard')}}">
							<a  href="{{route('admin.dashboard')}}" class="collapsed" aria-expanded="false">
								<i class="bi bi-house"></i>
								<p>Dashboard</p>
								
							</a>
							
						</li>
						
						<div class="clearfix"></div>
						<li class="nav-item mb-2 {{activeMenu('admin/programmers')}} {{activeMenu('admin/jobs')}} {{activeMenu('admin/events')}} {{activeMenu('admin/applied-job')}} {{activeMenu('admin/edit-event/*')}} {{activeMenu('admin/add-job')}}">
							<a data-toggle="collapse" href="#base">
								<i class="bi bi-layers"></i>
								<p>Jobs Setting</p>
								<span class="caret"></span>
							</a>
							<div class="collapse {{active('admin/programmers')}} {{active('admin/jobs')}} {{active('admin/events')}} {{active('admin/applied-job')}} {{active('admin/edit-event/*')}} {{active('admin/add-job')}}" id="base">
								<ul class="nav nav-collapse  mb-0 pb-0">
									<li class="{{activeMenu('admin/programmers')}}">
										<a href="{{route('admin.programmers')}}">
											<span class="sub-item">Programmes</span>
										</a>
									</li>
									<li class="{{activeMenu('admin/jobs')}} {{activeMenu('admin/add-job')}}">
										<a href="{{route('job.index')}}">
											<span class="sub-item">Jobs</span>
										</a>
									</li>
									<li class="{{activeMenu('admin/events')}} {{activeMenu('admin/edit-event/*')}} ">
										<a href="{{route('admin.event.index')}}">
											<span class="sub-item">Events</span>
										</a>
									</li>
									<li class="{{activeMenu('admin/applied-job')}}">
										<a href="{{route('admin.applied_job')}}">
											<span class="sub-item">Applied Jobs</span>
										</a>
									</li>
									<li class="{{activeMenu('admin/job-seeker')}}">
										<a href="{{route('admin.job_seeker.index')}}">
											<span class="sub-item">Job Seekers</span>
										</a>
									</li>
									<li class="{{activeMenu('admin/entrepreneur')}}">
										<a href="{{route('admin.entrepreneur.index')}}">
											<span class="sub-item">Entrepreneurs</span>
										</a>
									</li>
								</ul>
							</div>
						</li>
						<li class="nav-item mb-2 {{activeMenu('admin/setting')}}">
							<a data-toggle="collapse" href="#sidebarLayouts">
								<i class="bi bi-gear"></i>
								<p>Setting</p>
								<span class="caret"></span>
							</a>
							<div class="collapse {{active('admin/setting')}}" id="sidebarLayouts">
								<ul class="nav nav-collapse mb-0 pb-0">
									<li class="{{activeMenu('admin/setting')}}">
										<a href="{{url('/admin/setting')}}">
											<span class="sub-item">Setting</span>
										</a>
									</li>
									
									
								</ul>
							</div>
						</li>
						<li class="nav-item mb-2 {{activeMenu('admin/create-mail')}}">
							<a data-toggle="collapse" href="#sidebarLayouts1">
								<i class="bi bi-envelope"></i>
								<p>Mails</p>
								<span class="caret"></span>
							</a>
							<div class="collapse{{active('admin/create-mail')}}" id="sidebarLayouts1">
								<ul class="nav nav-collapse  mb-0 pb-0">
									<li class="{{activeMenu('admin/create-mail')}}">
										<a href="{{route('admin.mail.create')}}">
											<span class="sub-item">Mails</span>
										</a>
									</li>
									
									
								</ul>
							</div>
						</li>

						<li class="nav-item mb-2 {{activeMenu('admin/resouce/*')}}">
							<a data-toggle="collapse" href="#sidebarLayouts2">
								<i class="bi bi-card-checklist"></i>
								<p>Resources</p>
								<span class="caret"></span>
							</a>
							<div class="collapse {{active('admin/resouce/*')}}" id="sidebarLayouts2">
								<ul class="nav nav-collapse  mb-0 pb-0">
									<li class="{{activeMenu('admin/resouce')}}">
										<a href="{{route('admin.resource.index')}}">
											<span class="sub-item">Add Resource</span>
										</a>
										
									</li>
									<li class="{{activeMenu('admin/resouce/list')}} {{activeMenu('admin/resouce/edit/*')}}">
										<a href="{{route('admin.resource.list')}}">
											<span class="sub-item">Resource List</span>
										</a>
										
									</li>
									
									
								</ul>
							</div>
						</li>
						<li class="nav-item mb-2 {{activeMenu('admin/blog*')}} ">
							<a data-toggle="collapse" href="#sidebarLayouts3">
								<i class="bi bi-sticky"></i>
								<p>Blogs</p>
								<span class="caret"></span>
							</a>
							<div class="collapse {{active('admin/blog*')}} " id="sidebarLayouts3">
								<ul class="nav nav-collapse  mb-0 pb-0">
									<li class="{{activeMenu('admin/blog')}}">
										<a href="{{route('admin.blog.index')}}">
											<span class="sub-item">Add Blog</span>
										</a>
										
									</li>
									<li class="{{activeMenu('admin/blog/list')}} {{activeMenu('admin/blog/edit/*')}}">
										<a href="{{route('admin.blog.list')}}">
											<span class="sub-item">Blog List</span>
										</a>
										
									</li>
									
									
								</ul>
							</div>
						</li>
						<li class="nav-item mb-2 {{activeMenu('admin/community*')}} ">
							<a data-toggle="collapse" href="#sidebarLayouts4">
								<i class="bi bi-box-seam"></i>
								<p>Coummunity</p>
								<span class="caret"></span>
							</a>
							<div class="collapse {{active('admin/community*')}} " id="sidebarLayouts4">
								<ul class="nav nav-collapse  mb-0 pb-0">
									<li class="{{activeMenu('admin/community/add')}}">
										<a href="{{route('admin.blog.index')}}">
											<span class="sub-item">Add Community</span>
										</a>
										
									</li>
									<li class="{{activeMenu('admin/community')}} {{activeMenu('admin/community/edit/*')}}">
										<a href="{{route('admin.community.list')}}">
											<span class="sub-item">Community List</span>
										</a>
										
									</li>
									
									
								</ul>
							</div>
						</li>

						<li class="nav-item mb-2 {{activeMenu('admin/team-member*')}} ">
							<a data-toggle="collapse" href="#sidebarLayouts5">
								<i class="bi bi-people"></i>
								<p>Team Member</p>
								<span class="caret"></span>
							</a>
							<div class="collapse {{active('admin/team-member*')}} " id="sidebarLayouts5">
								<ul class="nav nav-collapse  mb-0 pb-0">
									<li class="{{activeMenu('admin/team-members')}}">
										<a href="{{route('admin.team_member.add')}}">
											<span class="sub-item">Add Team Member</span>
										</a>
										
									</li>
									<li class="{{activeMenu('admin/team_members')}} {{activeMenu('admin/team-members/edit/*')}}">
										<a href="{{route('admin.team_member.index')}}">
											<span class="sub-item">Team Member List</span>
										</a>
										
									</li>
								</ul>
							</div>
						</li>

						<li class="nav-item mb-2 {{activeMenu('admin/banner*')}} {{activeMenu('admin/profile*')}} ">
							<a data-toggle="collapse" href="#sidebarLayouts7">
								<i class="bi bi-house"></i>
								<p>Homepage Settings</p>
								<span class="caret"></span>
							</a>
							<div class="collapse {{active('admin/banner*')}} {{active('admin/profile*')}}" id="sidebarLayouts7">
								<ul class="nav nav-collapse  mb-0 pb-0">
									<li class="{{activeMenu('admin/homepage-settings/banners*')}}">
										<a href="{{route('admin.banners')}}">
											<span class="sub-item">Banners</span>
										</a>
										
									</li>
									<li class="{{activeMenu('admin/homepage-settings/profiles*')}}">
										<a href="{{route('admin.profiles')}}">
											<span class="sub-item">Profiles</span>
										</a>
										
									</li>
								</ul>
							</div>
						</li>
						
						<li class="nav-item mb-2 {{activeMenu('admin/page*')}} ">
							<a data-toggle="collapse" href="#sidebarLayouts8">
								<i class="bi bi-card-text"></i>
								<p>Pages</p>
								<span class="caret"></span>
							</a>
							<div class="collapse {{active('admin/page*')}} " id="sidebarLayouts8">
								<ul class="nav nav-collapse  mb-0 pb-0">
									<li class="{{activeMenu('admin/pages')}}">
										<a href="{{route('admin.page.add')}}">
											<span class="sub-item">Add Page</span>
										</a>
										
									</li>
									<li class="{{activeMenu('admin/page')}} {{activeMenu('admin/page/edit/*')}}">
										<a href="{{route('admin.page.index')}}">
											<span class="sub-item">Page List</span>
										</a>
										
									</li>
								</ul>
							</div>
						</li>
						
					</ul>
				</div>
			</div>
		</div>