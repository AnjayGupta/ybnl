<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>@yield("title")</title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href="{{asset('admin_assets/assets/img/icon.ico')}}" type="image/x-icon"/>
    <!-- Option 1: Include in HTML -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
	<!-- Fonts and icons -->
	<script src="{{asset('admin_assets/assets/js/plugin/webfont/webfont.min.js')}}"></script>
	<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">

	<script>
		WebFont.load({
			google: {"families":["Lato:300,400,700,900"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['../assets/css/fonts.min.css']},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>

	<!-- CSS Files -->
	<link rel="stylesheet" href="{{asset('admin_assets/assets/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('admin_assets/assets/css/atlantis.min.css')}}">

	<!-- CSS Just for demo purpose, don't include it in your project -->
	<link rel="stylesheet" href="{{asset('admin_assets/assets/css/demo.css')}}">
	@yield("css")
	<style>
		*{
			font-family: 'Poppins', sans-serif !important;
			font-weight: 600;
		}
		.swal-wide{
    width:450px !important;
}
.sweet-alert.sweetalert-lg { width: 600px; }
#profile-modal .modal-dialog {
    
    margin: 10.75rem auto;
}
#profile-modal .modal-content {
    border-radius: 0.1rem!important;
    border: 0!important;
	top:40% !important;
}

#toast-container>div{
    opacity: 1 !important;
	box-shadow: 0 0 12px #000 !important
}
		
	</style>
</head>