<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>YBLN | Admin</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('admin_assets/font-awesome/css/font-awesome.min.css')}}">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="{{ asset('admin_assets/css/style.css')}}">
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
</head>

<body>

    <div class="sidebar">
        <div class="cme-global-brand mx-3 mt-4 mb-3">
            <div class="open-sidebar d-flex justify-content-between align-items-center w-100">
                <div class="text-white mb-0 logo">YBLN ADMIN</div>
                <div class="sidebar-toggle d-xl-flex d-none justify-content-center align-items-center">
                    <img src="{{ asset('admin_assets/images/navbar/hamburger.svg')}}" alt="">
                </div>
            </div>

        </div>
        <ul class="ps-0 mb-0 mt-4 mx-3">
            <li class="mb-2"><a href="dashboard.html" class="active d-flex align-items-center"><img
                        src="{{ asset('admin_assets/images/navbar/dashboard.svg')}}" alt="" class="img-fluid me-2"> Dashboard</a></li>
            
            <li class="mb-2" id="dropdown">
                <div class="d-flex align-items-center justify-content-between"><span><img
                            src="{{ asset('admin_assets/images/navbar/cms.svg')}}" alt="" class="img-fluid me-2"> CMS</span> <span><i
                            class="fa fa-angle-right"></i></span></div>
                <ul class="ps-0" style="display: none;background-color: #0076d3;">
                    <li><a href="{{route('admin.programmers')}}"><i class="fa fa-angle-double-right"></i> programmes</a>
                    </li>
                    <li><a href="{{route('job.index')}}"><i class="fa fa-angle-double-right"></i> Jobs</a></li>
                    <li><a href="{{route('admin.event.index')}}"><i class="fa fa-angle-double-right"></i> Events</a></li>
                </ul>
            </li>

            <form method="POST" action="{{ route('logout') }}">
                            @csrf
                            <a href="{{route('logout')}}"  onclick="event.preventDefault();
                                                this.closest('form').submit();" class="d-flex align-items-center"><img
                        src="{{ asset('admin_assets/images/navbar/logout.svg')}}" alt="" class="img-fluid me-2"> Logout</a></li>

                        </form>

        </ul>
    </div>
    <nav class="navbar bg-white fixed-top py-sm-2 py-0" id="header">
        <div class="container-fluid">
            <div class="sidebar-toggle d-flex d-xl-none justify-content-center align-items-center">
                <img src="{{ asset('admin_assets/images/navbar/hamburger.svg')}}" alt="">
            </div>
            <ul class="navbar-nav justify-content-end flex-row flex-grow-1 align-items-center">
                <li class="nav-item mx-2 my-lg-2 my-xl-0">
                    <a class="nav-link" href="#"><img src="{{ asset('admin_assets/images/user-admin.png')}}" alt="" class="img-fluid"></a>
                </li>
            </ul>
        </div>
    </nav>
    {{ $slot }}

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.3.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
    
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>



    <script>
        $(document).ready(function () {
            $(".sidebar-toggle").click(function () {
                $(".sidebar").toggleClass("sidebar-open");
                $("#dropdown ul").removeClass("d-block dropdown-open");
                $("#dropdown .fa-angle-right").removeClass("rotate-icon");
                $("#dropdown div").removeClass("bg-color-black");
            })
            $("#dropdown").click(function () {
                $("#dropdown .fa-angle-right").toggleClass("rotate-icon")
                $("#dropdown div").toggleClass("bg-color-black");
                $("#dropdown ul").toggleClass("d-block dropdown-open");
            })
        })
    </script>
    <script>
        $(document).ready(function () {
            $('#recentCMEAdded').DataTable({
                searching: false,
                // paging: false, info: false
            });
        });
    </script>
    <script>
        $(document).ready(function () {
            $(".dataTables_filter input").attr('placeholder', 'Search');
        })
    </script>
        <script>
        $('.summernote').summernote({
            placeholder: 'Enter your text...',
            tabsize: 2,
            height: 500,
            dialogsFade: true,
            disableDragAndDrop: true,
            spellCheck: true,
            disableGrammar: true,
            blockquoteBreakingLevel: 2,
            fontSizeUnits: ['px', 'pt'],
            styleTags: [
                'p',
                { title: 'Blockquote', tag: 'blockquote', className: 'blockquote', value: 'blockquote' },
                'pre', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'
            ],
            popover: {
                image: [
                    ['image', ['resizeFull', 'resizeHalf', 'resizeQuarter', 'resizeNone']],
                    ['float', ['floatLeft', 'floatRight', 'floatNone']],
                    ['remove', ['removeMedia']]
                ],
                link: [
                    ['link', ['linkDialogShow', 'unlink']]
                ],
                table: [
                    ['add', ['addRowDown', 'addRowUp', 'addColLeft', 'addColRight']],
                    ['delete', ['deleteRow', 'deleteCol', 'deleteTable']],
                ],
                air: [
                    ['color', ['color']],
                    ['font', ['bold', 'underline', 'clear']],
                    ['para', ['ul', 'paragraph']],
                    ['table', ['table']],
                    ['insert', ['link', 'picture']]
                ]
            },
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear', 'strikethrough', 'superscript', 'subscript']],
                ['fontname', ['fontname']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']],
            ]
        });
    </script>
</body>

</html>