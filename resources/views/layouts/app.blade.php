<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- Primary Meta Tags -->
    <title>@yield('title', 'YBLN | Young Business Leaders of Nigeria')</title>
    <meta name="description" content="YBLN | Young Business Leaders of Nigeria" />
    <meta name="keywords" content="" />
    <meta charset="utf-8" />
    <meta name="author" content="YBLN | Young Business Leaders of Nigeria" />
    <meta name="title" content="YBLN | Young Business Leaders of Nigeria" />
    <meta name="description" content="YBLN | Young Business Leaders of Nigeria." />
    <!-- favicon -->
    <link rel="shortcut icon" href="{{ asset('assets/images/favicon.png') }}" />
    <link rel="apple-touch-icon" href="{{ asset('assets/images/apple-touch-icon-57x57.png') }}" />
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('assets/images/apple-touch-icon-72x72.png') }}" />
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('assets/images/apple-touch-icon-114x114.png') }}" />
    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://www.yblnigeria.com/" />
    <meta property="og:site_name" content="YBLN" />
    <meta property="og:title" content="YBLN | Young Business Leaders of Nigeria" />
    <meta property="og:description" content="YBLN | Young Business Leaders of Nigeria" />
    <!--        <meta property="og:image" content="https://metatags.io/assets/meta-tags-16a33a6a8531e519cc0936fbba0ad904e52d35f34a46c97a2c9f6f7dd7d336f2.png">-->
    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image" />
    <meta property="twitter:url" content="https://www.yblnigeria.com/" />
    <meta property="twitter:title" content="YBLN | Young Business Leaders of Nigeria" />
    <meta property="twitter:description" content="YBLN | Young Business Leaders of Nigeria." />
    <!--        <meta property="twitter:image" content="https://metatags.io/assets/meta-tags-16a33a6a8531e519cc0936fbba0ad904e52d35f34a46c97a2c9f6f7dd7d336f2.png">-->
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "WebSite",
            "@id": "http://www.yblnigeria.com/#website",
            "url": "http://www.yblnigeria.com/",
            "name": "YBLN | Young Business Leaders of Nigeria": {
                "@type": "SearchAction",
                "target": "http://www.yblnigeria.com/?s={search_term_string}",
                "query-input": "required name=search_term_string"
            }
        }

    </script>
    <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="dns-prefetch" href="//s.w.org" />
    <link href="https://fonts.googleapis.com/css2?family=Manrope:wght@400;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/style/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/style/modal-video.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/style/slick.css') }}" />

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/style/slick-theme.css') }}" />
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/style/hamburger.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/style/index.css') }}" />
    <style>
        .filter {
            max-width: 290px;
            margin-left: auto;

        }

        .filter input.form-control {
            padding: 12px;
            height: 48px;
            box-shadow: none;
        }

        .filter .fa {
            position: absolute;
            top: 0px;
            color: rgb(208, 203, 216);
            right: 0px;
            cursor: pointer;
            width: 48px;
            height: 48px;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .loader {
            width: 48px;
            height: 48px;
            border: 5px solid #e8e4e4;
            border-bottom-color: transparent;
            border-radius: 50%;
            display: inline-block;
            box-sizing: border-box;
            animation: rotation 1s linear infinite;
        }

        @keyframes rotation {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }

        .loader-box {
            width: 100%;
            height: 100vh;
            position: fixed;
            overflow: hidden;
            z-index: 999999;
            top: 0;
            left: 0;
            ;
            display: flex;
            justify-content: center;
            align-items: center;
            background: rgba(0, 0, 0, 0.4)
        }
    </style>
    @yield('css')
</head>

<body>

    <div class="banner position-relative">
        <div class="top--nav">
            <div class="container d-none d-lg-block">
                <div class="row">
                    <div class="col-lg-10">
                        <p class="my-3 text-white font--12">
                            <a href="mailto:info@yblnigeria.com" class="text-white"><i class="fa fa-envelope"
                                    aria-hidden="true"></i>
                                info@yblnigeria.com</a>
                        </p>
                    </div>

                    <div class="col-lg-2 text-right">
                        <p class="my-3 font--12">
                            <a href="tel:+2347088473165" class="text-white"><i class="fa fa-phone"
                                    aria-hidden="true"></i> +234 708
                                847 3165</a>
                        </p>
                    </div>
                </div>

                <hr class="top__divider my-0" />
            </div>
        </div>
        <nav class="navbar active_nav navbar-expand-lg navbar-light py-md-0 fixd-top bg-white">
            <div class="container">
                <a class="navbar-brand" href="/"><img loading="lazy" src="{{ asset('assets/images/logo.png') }}"
                        alt="yblnigeria Logo" class="" width="100" /></a>
                <button class="hamburger hamburger--emphatic navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarNav" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav ml-auto landing-page">
                        <li class="nav-item mx-md-3 my-md-0 postion-relative">
                            <a class="nav-link px-0 {{ Request::path() == '/' ? 'active' : '' }}"
                                href="{{ route('home') }}">HOME</a>
                        </li>
                        <li class="nav-item mx-md-3 my-md-0 postion-relative">
                            <a class="nav-link px-0 {{ Request::path() == 'about-us' ? 'active' : '' }}"
                                href="{{ route('about-us') }}">ABOUT US</a>
                        </li>

                        {{-- <li class="nav-item mx-md-3 my-md-0 postion-relative">
                            <a class="nav-link px-0 {{ Request::path() == 'programmers' ? 'active' : '' }}" href="{{route('programmes')}}">PROGRAMMES</a>
                        </li> --}}

                        <li class="nav-item mx-md-3 my-md-0 postion-relative">
                            <a class="nav-link px-0 {{ Request::path() == 'programmers' ? 'active' : '' }}"
                                href="{{ route('programmes') }}">WHAT WE DO?</a>
                        </li>

                        <li class="nav-item mx-md-3 my-md-0 postion-relative">
                            <a class="nav-link px-0 @if (Request::path() == 'jobs' || Request::path() == 'job-details') active @endif"
                                href="{{ route('jobs') }}">JOB MARKET</a>
                        </li>

                        {{-- <li class="nav-item mx-md-3 my-md-0 postion-relative">
                            <a class="nav-link px-0 {{ Request::path() == 'resources' ? 'active' : '' }}" href="{{route('resources')}}">RESOURCES</a>
                        </li> --}}
                        <li class="nav-item mx-md-3 my-md-0 postion-relative">
                            <label class="dropdown nav-link">

                                <div class="dd-button">
                                    Resource
                                </div>

                                <input type="checkbox" class="dd-input" id="test">

                                <ul class="dd-menu">
                                    <li><a href="{{ route('job-seekers') }}">Job Seekers</a></li>
                                    <li><a href="{{ route('entrepreneurs') }}">ENTREPRENEURS </a></li>
                                </ul>

                            </label>
                        </li>
                        <li class="nav-item mx-md-3 my-md-0 postion-relative">
                            <label class="dropdown nav-link">

                                <div class="dd-button">
                                    Media
                                </div>

                                <input type="checkbox" class="dd-input" id="test">

                                <ul class="dd-menu">
                                    <li><a href="{{ route('blogs') }}">BLOG</a></li>
                                    <li><a href="{{ route('events') }}">EVENTS</a></li>
                                </ul>

                            </label>
                        </li>
                        {{-- <li class="nav-item mx-md-3 my-md-0 postion-relative">
                            <a class="nav-link px-0 @if (Request::path() == 'blogs' || Request::path() == 'blog-details') active @endif" href="{{route('blogs')}}">BLOG</a>
                        </li>

                        <li class="nav-item mx-md-3 my-md-0 postion-relative">
                            <a class="nav-link px-0 {{ Request::path() == 'event' ? 'active' : '' }}" href="{{route('events')}}">EVENT</a>
                        </li> --}}

                        <li class="nav-item mx-md-3 my-md-0 postion-relative d-lg-none">
                            <a href="" class="nav-link px-0 text-uppercase">Join Commuity</a>
                        </li>

                        <li class="dropdown nav-item my-md-0 postion-relative d-none d-lg-block">
                            <a target="_SELF" href="{{ route('join.community') }}"
                                class="btn btn__primary text-uppercase p-3  px-5 ml-lg-4 my-2 d-block d-lg-inline text-uppercase">Join
                                the
                                Community</a>
                        </li>

                    </ul>
                </div>
            </div>
        </nav>
    </div>

    @yield('content')

    <!-- footer area -->
    <div class="footer">
        <div class="container">
            <div class="row pt-5">
                <div class="col-lg-3 text-center col-sm-6 col-12 about-widget footer-information">
                    <div class="text-center">
                        <img src="{{ asset('assets/images/logo-white.png') }}" alt="" class="img-fluid"
                            width="120">
                    </div>
                    <ul class="list-inline mt-3">
                        <li class="list-inline-item mr-4">
                            <a href="{{ $adminSettings->facebook_link ? $adminSettings->facebook_link : '#!' }}" target="_blank"><img
                                    src="{{ asset('assets/images/facebook.svg') }}" alt="follow on facebook"
                                    class="img-fluid"></a>
                        </li>
                        <li class="list-inline-item mr-4">
                            <a href="{{ $adminSettings->youtube_link ? $adminSettings->youtube_link : '#!' }}" target="_blank"><img
                                    src="{{ asset('assets/images/instagram.svg') }}" width="35" alt="follow on Instagram"
                                    class="img-fluid"></a>
                        </li>
                        <li class="list-inline-item mr-4">
                            <a href="{{ $adminSettings->twitter_link ? $adminSettings->twitter_link : '#!' }}" target="_blank"><img
                                    src="{{ asset('assets/images/twitter.svg') }}" alt="follow on twitter"
                                    class="img-fluid"></a>
                        </li>
                        <li class="list-inline-item">
                            <a href="{{ $adminSettings->linkedin ? $adminSettings->linkedin : '#!' }}" target="_blank"><img
                                    src="{{ asset('assets/images/linkedin.svg') }}" alt="follow on linkedin"
                                    class="img-fluid"></a>
                        </li>
                    </ul>
                </div>
                <!-- /.about-widget -->
                <div class="col-lg-3 col-sm-6 col-12 footer-list">
                    <h5 class="footer__title">About YBLN</h5>
                    <ul class="list-unstyled">
                        <li class="my-2"><a href="{{ route('about-us') }}">What we do</a></li>
                        <li class="my-2"><a href="{{ route('contact-us') }}">Contact Us</a></li>
                    </ul>

                    <h5 class="footer__title mt-5">Useful Links</h5>
                    <ul class="list-unstyled">
                        <li class="my-2"><a href="{{ route('jobs') }}">Job Boards</a></li>
                    </ul>
                </div>

                <div class="col-lg-3 col-sm-6 col-12 footer-list">
                    <h5 class="footer__title">YBLN Programmes</h5>
                    <ul class="list-unstyled">
                        <li class="my-2"><a href="{{ route('events') }}">YBNL Events</a></li>
                        <!--                            <li class="my-2"><a href="about.html"></a></li>-->
                        <br>
                    </ul>

                    <h5 class="footer__title mt-5">Articles & Resources</h5>
                    <ul class="list-unstyled">
                        <li class="my-2"><a href="{{ route('resources') }}">Resources</a></li>
                        <li class="my-2"><a href="{{ route('blogs') }}">Blogs</a></li>
                    </ul>
                </div>


                <div class="col-lg-3 col-sm-6 col-12 footer-list">
                    <h5 class="footer__title">Join Our Newsletter</h5>
                    <p class="footer__description">Subscribe to our newsletter.</p>
                    <form method="POST">

                        <input required="" name="email" type="email" class="form-control"
                            placeholder="Enter your email address">

                        <button type="submit" class="btn btn__primary my-3">Subscribe</button>

                    </form>

                </div>
                <!-- /.footer-list -->
            </div>
        </div>
        <div class="container">
            <hr class="bg-white">
            <div class="row">
                <div class="col-lg-6 col-sm-6 col-12 about-widget footer-information">
                    <p class="footer__description">Copyright 2022 YBLN, All Right Reserved.</p>
                </div>
                <div class="col-lg-3 col-sm-6 col-12 about-widget footer-information text-lg-right">
                    <p class="footer__description"><a href="{{ route('page', 'privacy-policy') }}" class="">Privacy Policy</a></p>
                </div>
                <div class="col-lg-3 col-sm-6 col-12 about-widget footer-information text-lg-right">
                    <p class="footer__description"><a href="{{ route('page', 'terms-conditions') }}">Terms & Conditions</a></p>
                </div>
            </div>
        </div>
    </div>
</body>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
    integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous">
</script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Counter-Up/1.0.0/jquery.counterup.min.js"></script>
<script src="{{ asset('assets/js/jquery-modal-video.js') }}"></script>
<script src="{{ asset('assets/js/slick.min.js') }}"></script>
<script>
    ajax = function(url, data) {
        return $.ajax({
            type: "GET",
            url: url,
            data: data,
        });
    }

    showLoader = function(box = "body") {
        var html = `<div class="loader-box">
        <span class="loader"></span>
    </div>`;
        $(box).append(html);
    }

    closeLoader = function() {
        $(document).find(".loader-box").remove();
    }
</script>


<script>
    $(document).ready(function() {
        AOS.init();

        $('.counter').counterUp({
            delay: 10,
            time: 1000
        });


        $(".js-modal-btn").modalVideo();

        $('.initiatives-items').slick({
            dots: true,
            infinite: false,
            speed: 300,
            slidesToShow: 3,
            slidesToScroll: 1,
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    },
                }, {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });


        $('.partners-items').slick({
            infinite: true,
            slidesToShow: 6,
            slidesToScroll: 1,
            // mobileFirst: true,
            //add this one
            //            autoplay: true,
            responsive: [{
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 320,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });


        $('.media_items').slick({
            infinite: true,
            slidesToShow: 2,
            slidesToScroll: 1,
            autoPlay: true,
            autoplaySpeed: 500,
            dots: true,
            //            mobileFirst: true,
            //add this one
            responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 320,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        });
        
    })
</script>
{{-- <script>
    $(document).ready(function() {
        $(".dropdown-toggle").click(function() {
            if ($(".dropdown").hasClass("show")) {
                $(".dropdown").removeClass("show")
                $(".dropdown-toggle").removeClass("show")
                $(".dropdown-menu").removeClass("show")
            } else {
                $(".dropdown").addClass("show")
                $(".dropdown-toggle").addClass("show")
                $(".dropdown-menu").addClass("show")
            }
        })
    })
</script> --}}
@yield('js')

</html>
