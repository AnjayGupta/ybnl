@extends('layouts.app')
@section('title','Resource')
@section('content')
<section class="page__header resources__header py-5 position-relative">
        <div class="container py-3">
            <div class="row">
                <div class="col-lg-8">
                    <p class="text-white sub mb-2 mt-5">GET THE HELP YOU NEED</p>
                    <h1 class="text-white mb-4">Resources</h1>
                    <p class="text-white mb-5">The latest documentations from the Young Business Leaders of Nigeria</p>
                </div>
            </div>

        </div>
        <img src="{{ asset('assets/images/page_abstract.svg')}}" alt="" class="page_abstract">
    </section>


    <section class="py-5">
        <div class="container">

            <div class="row">
                <div class="col-lg-8 my-4">
                    <h2 class="text-dark section__title font-weight-bold mb-2">Download your documents</h2>
                    <p class="section__sub mb-2">Find the documents your interested in.</p>
                </div>
            </div>
<!--
<div class="row job_actions my-4">
    <div class="col-lg-3 my-2">
        <select name="" id="" class="form-control">
            <option value="">All Category</option>
        </select>
    </div>

    <div class="col-lg-9 my-2">
        <input type="text" placeholder="Search Documents" class="form-control">
    </div>
</div>
-->
<div class="col-12">
                    <div class="filter">
                        <div class="form-group position-relative">
                            <input type="text" class="form-control search-box" id="search" placeholder="Search">
                            <i class="fa fa-search"></i>
                          </div>
                    </div>
                </div>
                <div class="row mb-2 box">
@foreach ($data as $list )
           
                <div class="col-lg-12 my-2">

                    <div class="job_card px-4 py-3 position-relative">
                       
                       <div class="row align-items-center">
                        <div class="col-lg-6">
                            <p class="title mb-0">
                                {{$list->title}}
                            </p>
                            <p class="location  mb-0">{!! Str::limit($list->desc, 300, '') !!}</p>
                        </div>
                        <div class="col-lg-6 text-right">
                            <a class="btn btn__primary" href="{{$list->docs}}" download=" {{$list->title}}">Download</a>
                        </div>
                    </div>
                           
                      

                    </div>

                </div>

           
            @endforeach
        </div>
            <div class="row">
                <div class="col-sm-12">
                    {{$data->links()}}

                </div>
            </div>
        </div>
    </section>
@endsection

@section("js")
<script>
    
$(".search-box").on("input",function(){
     
    var ajaxCall=ajax("{{url()->current()}}",{search:$(this).val().trim()});
    showLoader();
    ajaxCall.done(function(response){
           $(".box").html(response.result);
           closeLoader();
      });
       ajaxCall.fail(function(response){
       console.log(response);
      });

});
   
</script>
@endsection