@extends('layouts.app')
@section('title','Programmes')
@section("css")
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
@endsection
@section('content')

<section class="page__header programmes__header py-5 position-relative">
        <div class="container py-3">
            <div class="row">
                <div class="col-lg-8">
                    <p class="text-white sub mb-2 mt-5">PROGRAMMES</p>
                    <h1 class="text-white mb-4">What we Offer</h1>
                    <p class="text-white mb-5">The programmes offered in Young Business Leaders of Nigeria, YBLN,a
                        designed to help and assist in the development of youths in being successful enterpreneurs, get
                        access to gainful employements and get the necessay skills for business and to enter the job
                        market.</p>
                </div>
            </div>

        </div>
        <img src="{{ asset('assets/images/page_abstract.svg')}}" alt="" class="page_abstract">
    </section>



    <section class="py-5">
        <div class="container my-5">
            <div class="row">
                <div class="col-lg-12">
                    <p>Our YBLN Budding Entrepreneurs Pro- gram (BEP) is a well thought out entrepreneur- ship training
                        program designed to develop specific business skills in young people. Young individuals with
                        viable business ideas or existing businesses from diverse socio-economic backgrounds are
                        selected into a 6 weeks business incubation program and provided the necessary knowledge and
                        support to succeed in todays business world.</p>

                    <p> We strategize on an effective approach that attacks unemployment from two dimensions</p>

                    <ul>
                        <li>Focus on the Industry with the highest capacity to create wealth and provide multiple
                            streams of employment</li>
                        <li> Start from areas where unemployment is most predominant</li>
                    </ul>
                </div>
            </div>
                
            <div class="col-12">
                <div class="filter">
                    <div class="form-group position-relative">
                        <input type="text" class="form-control search-box" id="search" placeholder="Search" >
                        <i class="fa fa-search"></i>
                      </div>
                </div>
            </div>
            <div class="row mt-5 box">
                
                @foreach ($data as $list )
                <div class="col-lg-6 my-3">
                    <div class="blog__card h-100">
                        <div class="blog-img">
                            <img src="{{ $list->image}}" alt="100 Women in Agriculture" class="img-fluid">
                        </div>
                        <div class="p-4">
                            <a href="{{route('programmes.details',$list->slug)}}" class="nav-item">
                                <h2 class="blog__title mb-3">{{$list->title}}</h2>
                            </a>
                            <p class="blog__excerpts">{!! Str::limit($list->desc, 300, '') !!} </p>

                            <button class="btn btn__eminence text-uppercase my-2">Join PROGRAM</button>
                            @if($list->docs!="" && $list->docs!=null)
                            <a class="btn btn__primary text-uppercase my-2" href="{{$list->docs}}" download="{{$list->title}}">View BROCHURE</a>
                            @else
                            <a class="btn btn__primary text-uppercase my-2" href="#">View BROCHURE</a>
                            @endif
                            

                        </div>
                    </div>
                </div>                    
                @endforeach
               


            </div>

            <div class="row my-4">
                <div class="col-sm-12">
                  <center>  {{$data->links()}}</center>
                </div>
            </div>
        </div>
    </section>

@endsection

@section("js")
<script>
    
$(".search-box").on("input",function(){
     
    var ajaxCall=ajax("{{url()->current()}}",{search:$(this).val().trim()});
    showLoader();
    ajaxCall.done(function(response){
           $(".box").html(response.result);
           closeLoader();
      });
       ajaxCall.fail(function(response){
       console.log(response);
      });

});
   
</script>
@endsection