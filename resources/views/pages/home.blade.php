@extends('layouts.app')
@section('title','YBLN | Young Business Leaders of Nigeria')
@section("css")

@endsection
@section('content')

<div class="hero-section position-relative">
    <div class="ybln-stories-swiper swiper mySwiper">
        <div class="swiper-wrapper">

            @foreach($banners as $banner)
                <div class="swiper-slide">
                    <div class="banner-slide" style="background-image: url('{{ $banner->cover_image }}'); background-position: center center; background-repeat: no-repeat; background-size: cover; position: relative;">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div
                                        class="hero-section-content h-100 d-flex flex-column justify-content-center text-white">
                                        <h6>{{ $banner->short_description }}</h6>
                                        <h2>{{ Str::upper($banner->title) }}</h2>
                                        <p>{{ $banner->long_description }}</p>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

            {{-- <div class="swiper-slide swiper-slide-2">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <div
                                class="hero-section-content h-100 d-flex flex-column justify-content-center text-white">
                                <h6>Young Business Leaders of Nigeria</h6>
                                <h2>HOW DO WE DO IT</h2>
                                <p>Capacity Building + Capital + Community for entrepreneurs to grow and generate a high Return on Employment (ROE)</p>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide swiper-slide-3">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <div
                                class="hero-section-content h-100 d-flex flex-column justify-content-center text-white">
                                <h6>Young Business Leaders of Nigeria</h6>
                                <h2>MEMBER SPOTLIGHT</h2>
                                <p>MEMBER SPOTLIGHT</p>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div> --}}
        </div>
    </div>

</div>
<div class="counter-part">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="counter-container">
                    <div class="row">
                        <div class="col-md-4 my-2">
                            <div class="counter-item text-center">
                                <h3 class="mb-0 green-color"><span id="num1">1718</span> +</h3>
                                <p class="mb-0">
                                    Entrepreneurs
                                </p>
                            </div>

                        </div>
                        <div class="col-md-4 my-2">
                            <div class="counter-item text-center">
                                <h3 class="mb-0 orange-color"><span id="num2">1211</span> +</h3>
                                <p class="mb-0">
                                    Employed
                                </p>
                            </div>
                        </div>
                        <div class="col-md-4 my-2">
                            <div class="counter-item text-center border-0">
                                <h3 class="mb-0 mehroom-color"><span id="num3">5117</span> +</h3>
                                <p class="mb-0">
                                    Unemployed
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- <section class="about-ylbn my-5 py-5">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="about-ylbn-content">
                    <div class="about-ylbn-img mb-3">
                        <img src="assets/images/about-ylbn.png" alt="">
                    </div>
                    <h4>YBLN is a not-for-profit network of young entrepreneurs and professionals established to
                        develop creative means of solving unemployment in Nigeria.</h4>
                    <p>YBLN is a focused attempt at getting a large mass of young Nigerians off the sidelines while
                        evolving and maintaining strategies that would help reduce unemployment rate across the
                        country.</p>
                    <a href="#">Learn More about us <i class="fa fa-long-arrow-right ml-3"></i></a>
                </div>
            </div>
        </div>
    </div>
</section> --}}

<section class="about-ylbn my-5 py-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="about-ybln-video position-relative">
                    <iframe width="100%" height="624" src="https://www.youtube.com/embed/5vy6QBsGdU8?autoplay=1&mute=1&loop=1&controls=0&playlist=5vy6QBsGdU8&rel=1&autohide=2&modestbranding=1&showinfo=0&enablejsapi=1&wmode=opaque&border=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                </div>
            </div>
            {{-- <div class="col-lg-6">
                <div class="about-ylbn-img">
                        <img src="assets/images/about_image.jpg" alt="" class="img-fluid">
                </div>
            </div> --}}
        </div>
    </div>
</section>

<div class="ybln-profile py-5">
    <div class="container">
        <div class="row">
            <div class="col-12 mb-4">
                <div class="heading">
                    <h2>YBLN Profile</h2>
                </div>
            </div>
            
            @foreach($profiles as $profile)
                <div class="col-lg-6 mb-4">
                    <div class="ybln-profile-item" style="background-image: url('{{ $profile->image }}'); box-shadow: inset 1px 300px 1px {{ $profile->overlay_color ? $profile->overlay_color : '#9e9c9cbd' }}">
                        <h4>{{ $profile->title }}</h4>
                        <p class="mb-0">{{ $profile->description }}</p>
                    </div>
                </div>
            @endforeach

        </div>
    </div>
</div>

<section class="our-programs py-5 mb-5">
    <div class="abstract-img"><img src="assets/images/initiatives-abstract.svg" alt="" class="img-fluid"></div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="heading">
                    <p>Opportunities to take advantage of</p>
                    <h2 class="green-color font-weight-bold mb-4">Our Initiatives</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="our-program-swiper swiper mySwiper">
        <div class="swiper-wrapper">
            @foreach ($program as $list )
            <div class="swiper-slide pb-5 mb-5">
                <a href="{{route('programmes.details',$list->slug)}}" class="text-decoration-none" target="_blank">
                    <div class="our-programs-content slide-1" style="background-image:url('{{$list->image}}')">
                        <h5>{{$list->title}}</h5>
                        <p class="mb-0">{!!\Str::limit($list->desc,100,'')!!}</p>
                    </div>
                </a>
            </div>
            @endforeach

        </div>
        <div class="swiper-pagination"></div>
    </div>
</section>

{{-- <section class="ybln-stories py-5">
    <div class="container">
        <div class="row">
            <div class="col-12 mb-3">
                <div class="ybln-stories-logo mb-3">
                    <img src="assets/images/ybln-stories.png" alt="" class="img-fluid">
                </div>
                <p class="text-white">Lorem ipsum dolar sit amet, consectetur adipiscing elit.</p>
            </div>
            <div class="col-12">
                <div class="ybln-stories-swiper swiper mySwiper">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide mb-5">
                            <div class="row">
                                <div class="col-lg-5 px-0">
                                    <div class="ybln-stories-img">
                                        <img src="assets/images/image_1.jpg" alt="" class="img-fluid w-100">
                                    </div>
                                </div>
                                <div class="col-lg-7 px-0">
                                    <div class="bg-white h-100 d-flex flex-column justify-content-center p-4">
                                        <h4>Mrs. Anita Abdulahi</h4>
                                        <p>CEO, Wood Factory</p>
                                        <p class="font-italic">Lorem ipsum dolor sit amet consectetur adipisicing
                                            elit. Quidem deleniti
                                            a quae quos rerum ut animi sit explicabo corporis, qui eaque voluptatem
                                            delectus molestias, laboriosam, necessitatibus illum accusamus ullam!
                                            Porro!</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide mb-5">
                            <div class="row">
                                <div class="col-lg-5 px-0">
                                    <div class="ybln-stories-img">
                                        <img src="assets/images/image_1.jpg" alt="" class="img-fluid w-100">
                                    </div>
                                </div>
                                <div class="col-lg-7 px-0">
                                    <div class="bg-white h-100 d-flex flex-column justify-content-center p-4">
                                        <h4>Mrs. Anita Abdulahi</h4>
                                        <p>CEO, Wood Factory</p>
                                        <p class="font-italic">Lorem ipsum dolor sit amet consectetur adipisicing
                                            elit. Quidem deleniti
                                            a quae quos rerum ut animi sit explicabo corporis, qui eaque voluptatem
                                            delectus molestias, laboriosam, necessitatibus illum accusamus ullam!
                                            Porro!</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
        </div>
    </div>
</section> --}}

<section class="ybln-articles py-5">
    <div class="container">
        <div class="row">
            <div class="col-12 mb-4">
                <div class="d-flex justify-content-between align-items-center">
                        <div class="heading">
                            <h2 class="green-color font-weight-bold">Our Articles</h2>
                        </div>
                    <div class="view-all-btn">
                        <a href="{{route('blogs')}}">View all <i class="fa fa-long-arrow-right"></i></a>
                    </div>
                </div>
            </div>
            @foreach ($blog as $blog_list )
            <div class="col-lg-4 col-md-6 mb-5">
                <div class="ybln-articles-item">
                    <a href="{{route('blog-details',$blog_list->slug)}}">
                        <div class="ybln-articles-img">
                            <img src="{{$blog_list->image}}" alt="" class="img-fluid w-100">
                        </div>
                    </a>
                    <div class="ybln-articles-content p-4">
                        <a href="{{route('blog-details',$blog_list->slug)}}" class="text-decoration-none">
                            <p class="mb-0"><span>{{$blog_list->title}}</span></p>
                        </a>
                        <p class="font-weight-bold"> {!!\Str::limit($blog_list->description,150,' <a href="'.route('blog-details',$blog_list->slug ).'" style="margin-right:10px;text-decoration:none;font-weight:bold">... read more</a>')!!}</p>
                        <p class="date mb-0">{{$blog_list->created_at->format('M d Y')}}</p>
                    </div>
                </div>
            </div>

            @endforeach

        </div>
    </div>
</section>

{{-- <section class="ybln-forum py-5">
    <div class="container">
        <div class="row">
            <div class="col-12 mb-4">
                <div class="d-flex justify-content-between mb-4">
                    <h5 class="primary-color font-weight-bold">YBLN Forum</h5>
                    <div class="view-all-btn">
                        <a href="#" class="font-weight-bold">View all <i class="fa fa-long-arrow-right"></i></a>
                    </div>
                </div>
                <p class="primary-color">Lorem ipsum dolor sit amet consectetur adipisicing elit. Possimus delectus
                    consequuntur assumenda
                    quis modi deserunt placeat maiores sed dignissimos repudiandae iusto ea ipsa dolore,
                    voluptatibus laboriosam eaque nulla.</p>
            </div>
            <div class="col-lg-4 mb-4">
                <div class="ybln-forum-item d-flex flex-column justify-content-between">
                    <div>
                        <h4 class="font-weight-bold primary-color">Enterpreneurship Meetup</h4>
                        <p>310 members</p>
                    </div>
                    <div class="ybln-forum-btn">
                        <a href="#">Join Community</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 mb-4">
                <div class="ybln-forum-item d-flex flex-column justify-content-between">
                    <div>
                        <h4 class="font-weight-bold primary-color">My unemployment Story</h4>
                        <p>20 members</p>
                    </div>
                    <div class="ybln-forum-btn">
                        <a href="#">Join Community</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 mb-4">
                <div class="ybln-forum-item d-flex flex-column justify-content-between">
                    <div>
                        <h4 class="font-weight-bold primary-color">Young Enterpreneurs Meetup</h4>
                        <p>157 members</p>
                    </div>
                    <div class="ybln-forum-btn">
                        <a href="#">Join Community</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 mb-4">
                <div class="ybln-forum-item d-flex flex-column justify-content-between">
                    <div>
                        <h4 class="font-weight-bold primary-color">Young Professionals</h4>
                        <p>119 members</p>
                    </div>
                    <div class="ybln-forum-btn">
                        <a href="#">Join Community</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 mb-4">
                <div class="ybln-forum-item d-flex flex-column justify-content-between">
                    <div>
                        <h4 class="font-weight-bold primary-color">Enterpreneurs Corner</h4>
                        <p>43 members</p>
                    </div>
                    <div class="ybln-forum-btn">
                        <a href="#">Join Community</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 mb-4">
                <div class="ybln-forum-item d-flex flex-column justify-content-between">
                    <div>
                        <h4 class="font-weight-bold primary-color">Mye Enterpreneurship Story</h4>
                        <p>119 members</p>
                    </div>
                    <div class="ybln-forum-btn">
                        <a href="#">Join Community</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> --}}

{{-- <section class="cta py-5 my-5">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-7 mb-5">
                <div class="cta-content">
                    <h3 class="font-weight-bold mb-4">Imperdiet consectetur in facilisis lacus imperdiet non turpis
                        feugiat.</h3>
                    <a href="#">Subscribe Today</a>
                </div>
            </div>
            <div class="col-lg-5 mb-5">
                <div class="cta-img">
                    <img src="assets/images/image_1.jpg" alt="" class="img-fluid">
                </div>
            </div>
        </div>
    </div>
</section> --}}



@endsection
@section("js")
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js"></script>
    <!-- Swiper JS -->
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
    <script src="{{asset('assets/js/countMe.min.js')}}"></script>

    <!-- Initialize Swiper -->
    <script>
        var swiper = new Swiper(".our-programs .our-program-swiper", {
            autoplay: {
                delay: 3500,
                disableOnInteraction: false,
            },
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
            },
            breakpoints: {
                640: {
                    slidesPerView: 2,
                    spaceBetween: 0,
                },
                1024: {
                    slidesPerView: 3,
                    spaceBetween: 0,
                },
            },
        });
    </script>
    <script>
        var swiper = new Swiper(".hero-section .ybln-stories-swiper", {
            navigation: {
                nextEl: ".hero-section .swiper-button-next",
                prevEl: ".hero-section .swiper-button-prev",
            },
            navigation: {
                nextEl: ".hero-section .swiper-button-next",
                prevEl: ".hero-section .swiper-button-prev",
            },
            autoplay: {
        delay: 3500,
        disableOnInteraction: false,
      },
            breakpoints: {
                0: {
                    slidesPerView: 1,
                    spaceBetween: 20,
                }
            },
        });
    </script>
    <script>
        window.onload = () => {
            // $(selector).countMe(delay,speed)
            $("#num1").countMe(0, 14);
            $("#num2").countMe(0, 20);
            $("#num3").countMe(0, 0);
        }
        $(document).ready(function () {
            $("#counter-item h3").insertAfter("+")
        })
    </script>

@endsection
