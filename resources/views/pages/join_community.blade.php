@extends("layouts.app")
@section("css")
{!! NoCaptcha::renderJs() !!}
@endsection
@section("content")
<section class="page__header jobs__header py-5 position-relative">
    <div class="container py-3">
        <div class="row">
            <div class="col-lg-8">
                <div class="page-title py-5">
                    <h1 class="text-white mb-4">GET THE HELP YOU NEED</h1>
                    <p class="text-white mb-5">The latest documentations from the Young Business Leaders of Nigeria</p>
                </div>
            </div>
        </div>

    </div>
    <img src="{{ asset('assets/images/page_abstract.svg')}}" alt="" class="page_abstract">
</section>

<section class="join-our-community py-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="heading">
                    <h2>Contact Us</h2>
                </div>
                <form class="row" action="{{route('join.community.store')}}" method="POST">
                    @csrf
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="firstName">First Name</label>
                            <input type="text" class="form-control" id="firstName" placeholder="First Name" name="first_name">
                            @if ($errors->has('first_name'))
                            <span class="help-block text-danger">
                            {{$errors->first("last_name")}}
                            </span>
                        @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="lastName">Last Name</label>
                            <input type="text" class="form-control" id="lastName" placeholder="Last Name" name="last_name">
                            @if ($errors->has('last_name'))
                            <span class="help-block text-danger">
                            {{$errors->first("last_name")}}
                            </span>
                        @endif
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="email" class="form-control" id="exampleInputEmail1"
                                aria-describedby="emailHelp" placeholder="Email address" name="email">
                                @if ($errors->has('email'))
                                <span class="help-block text-danger">
                                {{$errors->first("email")}}
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label for="phoneNumber">Phone Number</label>
                            <input type="number" class="form-control" id="phoneNumber" placeholder="Phone Number" name="phone">
                            @if ($errors->has('phone'))
                            <span class="help-block text-danger">
                            {{$errors->first("phone")}}
                            </span>
                        @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
                        <label class="col-md-4 control-label">Captcha</label>
                        <div class="col-md-12">
                            {!! app('captcha')->display() !!}
                            @if ($errors->has('g-recaptcha-response'))
                                <span class="help-block text-danger">
                                  Captacha is required
                                </span>
                            @endif
                        </div>
                    </div>
                   
                    <div class="col-12">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection