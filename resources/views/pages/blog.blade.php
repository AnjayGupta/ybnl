@extends('layouts.app')
@section('title','Blog')
@section('content')
    <section class="page__header blog__header py-5 position-relative">
        <div class="container py-3">
            <div class="row">
                <div class="col-lg-8">
                    <p class="text-white sub mb-2 mt-5">Learning Materials and resources</p>
                    <h1 class="text-white mb-4">Our Blogs</h1>
                    <p class="text-white mb-5">The latest updates, stories, ideas and guides from the Young Business
                        Leaders of Nigeria</p>
                </div>
            </div>

        </div>
        <img src="{{ asset('assets/images/page_abstract.svg')}}" alt="" class="page_abstract">
    </section>

    <!-- blog area -->
    <section class="blog_area py-5">
        <div class="container">

            <div class="row">
                <div class="col-lg-4 my-4">
                    {{-- <p class="section__sub mb-2">Blog</p> --}}
                    <h2 class="primary__text section__title font-weight-bold">Latest Articles</h2>
                </div>
            </div>
            <div class="col-12">
                <div class="filter">
                    <div class="form-group position-relative">
                        <input type="text" class="form-control search-box" id="search" placeholder="Search">
                        <i class="fa fa-search"></i>
                      </div>
                </div>
            </div>
            <div class="row blog-box" id="blog-box">
                
                @foreach ($data as  $item)
                <div class="col-lg-4 my-3">
                    <div class="blog__card h-100 d-flex flex-column">
                        <div class="blog-item-img">
                            <img src="{{$item->image}}" alt="" class="img-fluid">
                        </div>
                        <div class="p-4 d-flex flex-column justify-content-between blog-details">
                            <div>
                                <a href="{{route('blog-details',$item->slug)}}" class="text-decoration-none">
                                    <h2 class="blog__title mb-3">{{$item->title}}</h2>
                                </a>
                                <p class="blog__excerpts">{!!\Str::limit($item->description,300,' <a href="'.route('blog-details',$item->slug).'" style="margin-right:10px;text-decoration:none;font-weight:bold">... read more</a>')!!} </p>
                            </div>

                            <ul class="list-inline">
                                <li class="">
                                    <a href="#">Entrepreneurship</a>
                                </li>
                                <li class="text-right">{{$item->created_at->format("M d Y")}}</li>
                            </ul>
                        </div>
                    </div>
                </div>
                    
                @endforeach

                <div class="col-sm-12">
                    {{$data->links()}}
                </div>
                
            </div>
        </div>

    </section>
@endsection

@section("js")
<script>
    
$(".search-box").on("input",function(){
     
    var ajaxCall=ajax("{{url()->current()}}",{search:$(this).val().trim()});
    showLoader();
    ajaxCall.done(function(response){
           $("#blog-box").html(response.result);
           closeLoader();
      });
       ajaxCall.fail(function(response){
       console.log(response);
      });

});
   
</script>
@endsection