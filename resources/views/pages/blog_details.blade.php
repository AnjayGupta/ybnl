@extends('layouts.app')
@section('title')
{{$data->slug}}
@endsection
@section('content')
<section class="page__header blog__header py-5 position-relative">
        <div class="container py-3">
            <div class="row">
                <div class="col-lg-8">
                    <p class="text-white sub mb-2 mt-5">Learning Materials and resources</p>
                    <h1 class="text-white mb-4">Our Blogs</h1>
                    <p class="text-white mb-5">The latest updates, stories, ideas and guides from the Young Business
                        Leaders of Nigeria</p>
                </div>
            </div>

        </div>
        <img src="{{ asset('assets/images/page_abstract.svg')}}" alt="" class="page_abstract">
    </section>


    <section class="event_page py-5">
        <div class="container mt-5">
            <div class="row">
                <div class="col-lg-2 text-center d-lg-block d-none">
                    <p>Share</p>
                    <ul class="mt-3 list-unstyled">
                        <ul class="mt-3 list-unstyled">
                            <li class="my-4">
                                <a href="http://www.twitter.com/share?url={{url()->full()}}" target="_blank"><img src="{{ asset('assets/images/twitter-share.svg')}}" alt="share on twitter" class="img-fluid" rel="noopener"></a>
                            </li>
                            <li class="my-4">
                                <a href="https://www.facebook.com/sharer/sharer.php?u={{url()->full()}}" target="_blank"><img src="{{ asset('assets/images/facebook-share.svg')}}" alt="follow on facebook" class="img-fluid" rel="noopener"></a>
                            </li>
    
                            <li class="my-4">
                                <a href="https://www.linkedin.com/sharing/share-offsite/?url={{url()->full()}}" target="_blank"><img src="{{ asset('assets/images/linkedin-share.svg')}}" alt="share on linkedin" class="img-fluid" rel="noopener"></a>
                            </li>
    
                            <li class="my-4">
                                <a href="https://www.instagram.com/?url={{url()->full()}}" target="_blank"><img src="{{ asset('assets/images/instagram-share.svg')}}" alt="share on Instagram" class="img-fluid" rel="noopener"></a>
                            </li>
                        </ul>
                    </ul>
                </div>
                <div class="col-lg-9">
                    <h1 class="title mb-5">{{$data->title}}</h1>
                    <div class="mb-5">
                        <img src="{{ $data->image}}" class="img-fluid image" alt="">
                        
                    </div>

                    <p>{!!$data->description!!}</p>
                    <ul class="list-inline mt-3">
                       {{-- {!! social() !!} --}}
                    </ul>

                </div>
            </div>
        </div>
    </section>

@endsection