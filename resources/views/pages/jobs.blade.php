@extends('layouts.app')
@section('title','Job markets')
@section("css")
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
@endsection
@section('content')

<section class="page__header jobs__header py-5 position-relative">
        <div class="container py-3">
            <div class="row">
                <div class="col-lg-8">
                    <p class="text-white sub mb-2 mt-5">JOB MARKET</p>
                    <h1 class="text-white mb-4">Apply for Jobs</h1>
                    <p class="text-white mb-5">The programmes offered in Young Business Leaders of Nigeria, YBLN,a
                        designed to help and assist in the development of youths in being successful enterpreneurs, get
                        access to gainful employements and get the necessay skills for business and to enter the job
                        market.</p>
                </div>
            </div>

        </div>
        <img src="{{ asset('assets/images/page_abstract.svg')}}" alt="" class="page_abstract">
    </section>

    <section class="py-5">
        <div class="container">

            <div class="row">
                <div class="col-lg-8 my-4">
                    <h2 class="text-dark section__title font-weight-bold mb-2">Let’s find you an open position.</h2>
                    <p class="section__sub mb-2">Find the right job for you no matter what it is that you do.</p>
                </div>
            </div>

<!--
            <div class="row job_actions my-4">
                <div class="col-lg-3 my-2">
                    <select name="" id="" class="form-control">
                        <option value="">All locations</option>
                    </select>
                </div>

                <div class="col-lg-9 my-2">
                    <input type="text" placeholder="Search Positions" class="form-control">
                </div>
            </div>
-->
<div class="col-12">
    <div class="filter">
        <div class="form-group position-relative">
            <input type="text" class="form-control search-box" id="search" placeholder="Search">
            <i class="fa fa-search"></i>
          </div>
    </div>
</div>
            <div class="row job-box position-relative">
                
                @foreach($data as $val)
                <div class="col-lg-12 my-2">
                    <a href="{{ route('job-details',$val->slug)}}">
                        <div class="job_card px-4 py-3 position-relative">
                            <div class="row">
                                <div class="col-lg-6">
                                    <p class="title mb-0">
                                       {{$val->title}}
                                    </p>
                                    <p class="location  mb-0"> {{$val->location}}</p>
                                </div>
                                <div class="col-lg-6">
                                    <p class="department"> {{$val->sub_title}}</p>
                                </div>
                            </div>

                            <span class="position-absolute marker">
                                <svg width="9" height="12" viewBox="0 0 9 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M7.875 6.84766C8.19141 6.53125 8.19141 6.00391 7.875 5.65234L3.09375 0.871094C2.74219 0.554688 2.21484 0.554688 1.89844 0.871094L1.08984 1.67969C0.773438 2.03125 0.773438 2.55859 1.08984 2.875L4.5 6.28516L1.08984 9.66016C0.773438 9.97656 0.773438 10.5039 1.08984 10.8555L1.89844 11.6289C2.21484 11.9805 2.74219 11.9805 3.09375 11.6289L7.875 6.84766Z" fill="#66B22E" />
                                </svg>
                            </span>
                        </div>
                    </a>
                </div>
                @endforeach
                

                <div class="col-sm-12 mt-4">
                    {{$data->links()}}
                </div>
            </div>
        </div>
    </section>
@endsection
@section("js")
<script>
    
$(".search-box").on("input",function(){
     
    var ajaxCall=ajax("{{url()->current()}}",{search:$(this).val().trim()});
    showLoader();
    ajaxCall.done(function(response){
           $(".job-box").html(response.result);
           closeLoader();
      });
       ajaxCall.fail(function(response){
       console.log(response);
      });

});
   
</script>
@endsection