@extends('layouts.app')
@section('title','Job markets')
@section('content')
<section class="page__header events__header py-5 position-relative">
        <div class="container py-3">
            <div class="row">
                <div class="col-lg-8">
                    <p class="text-white sub mb-2 mt-5">ATTEND ONE OF OUR EVENTS</p>
                    <h1 class="text-white mb-4">YBLN Events</h1>
                    <p class="text-white mb-5">Upcoming events for you to attend</p>
                </div>
            </div>

        </div>
        <img src="{{ asset('assets/images/page_abstract.svg')}}" alt="" class="page_abstract">
    </section>


    <section class="event_page py-5">
        <div class="container mt-5">
            <div class="row">
                <div class="col-lg-2 text-center d-lg-block d-none  ">
                    <p>Share</p>
                    <ul class="mt-3 list-unstyled">
                        <ul class="mt-3 list-unstyled">
                            <li class="my-4">
                                <a href="http://www.twitter.com/share?url={{url()->full()}}" target="_blank"><img src="{{ asset('assets/images/twitter-share.svg')}}" alt="share on twitter" class="img-fluid" rel="noopener"></a>
                            </li>
                            <li class="my-4">
                                <a href="https://www.facebook.com/sharer/sharer.php?u={{url()->full()}}" target="_blank"><img src="{{ asset('assets/images/facebook-share.svg')}}" alt="follow on facebook" class="img-fluid" rel="noopener"></a>
                            </li>
    
                            <li class="my-4">
                                <a href="https://www.linkedin.com/sharing/share-offsite/?url={{url()->full()}}" target="_blank"><img src="{{ asset('assets/images/linkedin-share.svg')}}" alt="share on linkedin" class="img-fluid" rel="noopener"></a>
                            </li>
    
                            <li class="my-4">
                                <a href="https://www.instagram.com/?url={{url()->full()}}" target="_blank"><img src="{{ asset('assets/images/instagram-share.svg')}}" alt="share on Instagram" class="img-fluid" rel="noopener"></a>
                            </li>
                        </ul>
                    </ul>
                </div>
                <div class="col-lg-9">
                    <h1 class="title mb-5">{{$slugData->title}}</h1>
                    <div class="mb-5">
                        <img src="{{ $slugData->image }}" class="img-fluid image" alt="">
                        <div class="text-center">
                            <!-- <p class="image_caption text-center mt-2">This is a caption on this photo for reference</p> -->
                        </div>
                    </div>
                    {!! $slugData->desc !!}
                    {{-- <div class="row justify-content-center">
                        <div class="col-lg-9 my-5 text-center">
                            <button class="btn btn__primary btn-block text-uppercase">Register</button>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </section>
@endsection