@extends('layouts.app')
@section('title','About-Us')
@section("css")
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
@endsection
@section('content')

<section class="page__header py-5 position-relative" style="background-color: {{ $about_page->cover_color ? $about_page->cover_color : '#66B22E' }};">
        <div class="container py-3">
            <div class="row">
                <div class="col-lg-8">
                    <p class="text-white sub mb-2 mt-5">{{ $about_page->short_description }}</p>
                    <h1 class="text-white mb-4">{{ $about_page->title }}</h1>
                    <p class="text-white mb-5">{{ $about_page->long_description}}</p>
                </div>
            </div>

        </div>
        <img src="{{ $about_page->cover_image }}" alt="" class="page_abstract">
    </section>



    <section class="py-5 about_intro">

        <div class="container py-5">
            <div class="row  align-items-center">
                <div class="col-lg-6">
                    <p class="section__sub mb-2">What we do</p>
                    <h2 class="primary__text section__title font-weight-bold mb-3">Our Story</h2>

                    <p>Unemployment is a major issue affecting more than half of Nigeria’s population within the ages 18-35, with a higher rate in the northern part of the country. This fact is supported by data released by the National Bureau of Statistics, which revealed that the rate of unemployment in Nigeria increased to 7.50% in January 2015 from 6.40% in December 2014. </p>

                    <p>In a nutshell, the country has an unprecedented number of the population badly affected by the scourge of unemployment caused by corruption, mental and financial poverty, poor quality of education, lack of skills, lack of infrastructure and the lack of political will. Its far-reaching effects include political and pseudo-religious thuggery, restiveness, crime, poverty, low national industrial inputs and social inequality.</p>

                    <p>The Young Business Leaders of Nigeria was birthed in a bid to tackle the scourge of unemployment. Recognising the significant role that young people can play in helping solve this crisis, YBLN is a focused attempt at getting a large mass of young people off the sidelines while evolving and maintaining strategies and actions that would help reduce this.</p>

                    <p> Our key target is to reduce the rate of youth unemployment by 25% in the year 2020 </p>
                </div>
                <div class="col-lg-6">
                    <img src="{{ asset('assets/images/about-banner-image.png')}}" class="img-fluid" alt="">
                </div>
            </div>
        </div>
    </section>

    <section class="py-5 position-relative">
        <img src="{{ asset('assets/images/about_left.svg')}}" alt="" class="img-fluid about_left">
        <img src="{{ asset('assets/images/about_right.svg')}}" alt="" class="img-fluid about_right">
        <div class="container pt-5">
            <div class="row align-items-center mt-4">
                <div class="col-lg-6 my-4">
                    <h2 class="primary__text section__title font-weight-bold mb-3">Advisory Board</h2>
                </div>
            </div>

            <div class="row">
                @foreach($advisory_board_members as $member)
                    <div class="col-lg-4 my-3">
                        <div class="team-item">
                            <img src="{{ $member->image }}" class="img-fluid" alt="">
                        <div class="info__area px-3 py-2">
                            <p class="name mb-2">{{ Str::upper($member->name) }}</p>
                            <p class="position">{{ Str::upper($member->designation) }}</p>
                        </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="row align-items-center mt-4">
                <div class="col-lg-6 my-4">
                    <p class="section__sub text-dark mb-2">YBLN Team</p>
                    <h2 class="primary__text section__title font-weight-bold mb-3">Board Members</h2>
                </div>
            </div>


            <div class="row mb-5">
                @foreach($board_members as $member)
                    <div class="col-lg-4 my-3">
                        <div class="team-item">
                            <img src="{{ $member->image }}" class="img-fluid" alt="">
                            <div class="info__area px-3 py-2">
                                <p class="name mb-2">{{ Str::upper($member->name) }}</p>
                                <p class="position">{{ Str::upper($member->designation) }}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection