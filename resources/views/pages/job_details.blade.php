@extends('layouts.app')
@section('title',request()->segment(2))
@section('content')
<section class="page__header jobs__header py-5 position-relative">
        <div class="container py-3">
            <div class="row">
                <div class="col-lg-8">
                    <p class="text-white sub mb-2 mt-5">JOB MARKET</p>
                    <h1 class="text-white mb-4">Apply for Jobs</h1>
                    <p class="text-white mb-5">The programmes offered in Young Business Leaders of Nigeria, YBLN,a
                        designed to help and assist in the development of youths in being successful enterpreneurs, get
                        access to gainful employements and get the necessay skills for business and to enter the job
                        market.</p>
                </div>
            </div>

        </div>
        <img src="images/page_abstract.svg" alt="" class="page_abstract">
    </section>

    <section class="py-5">
        <div class="container">
            <div class="row mb-4">
                <div class="col-lg-4 mt-5">
                    <h2 class="text-dark section__title font-weight-bold mb-3">{{$slugData->title}}</h2>
                    <p class="section__sub text-dark mb-2">{{$slugData->location}} Full time</p>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 job_details">
                {!! $slugData->desc !!}
                </div>
            </div>
            <div class="row col-sm-12 py-4">
                @if (filter_var($slugData->link, FILTER_VALIDATE_EMAIL))
                <a href="mailto:{{$slugData->link}}" class="btn btn__primary text-uppercase p-3  px-5 ml-lg-4 my-2 d-block d-lg-inline text-uppercase">Apply Job</a>
                  @else
                  <a href="{{$slugData->link}}" class="btn btn__primary text-uppercase p-3  px-5 ml-lg-4 my-2 d-block d-lg-inline text-uppercase" target="_blank">Apply Job</a>
                @endif
                
            </div>
        </div>
       

    </section>

    {{-- <section class="job_details_form py-5 d-none" >
        <div class="container">
            <div class="row">
                <div class="col-lg-12" id="contactform">

                    <h2 class="my-5">Fill the information provided below</h2>

                    <form class="mb-5" action="{{route('job.applied')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="job_id" id="" value="{{$slugData->hashid}}">
                        <div class="form-row">
                            <div class="col-lg-6 my-3">
                                <label for="fullname">Full Name *</label>
                                <input type="text" id="fullname" class="form-control" placeholder="Full name" autocomplete="name" value="{{old('name')}}" name="name">
                                @error("name")
                                 <p class="text-danger font-weight-bold">{{$errors->first("name")}}</p>                                    
                                @enderror
                            </div>
                            <div class="col-lg-6 my-3">
                                <label for="email">Contact Email *</label>
                                <input type="email" id="email" class="form-control" placeholder="Contact email" autocomplete="email" name="email" value="{{old('email')}}">
                                @error("email")
                                <p class="text-danger font-weight-bold">{{$errors->first("email")}}</p>                                    
                               @enderror
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-lg-6 my-3">
                                <label for="subject">Subject *</label>
                                <input type="text" id="subject" class="form-control" placeholder="Subject" name="subject" value="{{old('subject')}}">
                                @error("subject")
                                <p class="text-danger font-weight-bold">{{$errors->first("subject")}}</p>                                    
                               @enderror

                            </div>
                            <div class="col-lg-6 my-3">
                                <label for="resume">Resume *</label>
                                <!-- <input type="file" id="resume" class="form-control"> -->
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="resume" name="resume">
                                    <label class="custom-file-label" for="resume">Choose file</label>
                                </div>
                                @error("resume")
                                <p class="text-danger font-weight-bold">{{$errors->first("resume")}}</p>                                    
                               @enderror
                            </div>
                        </div>


                        <div class="form-row">
                            <div class="col-lg-12 my-3">
                                <label for="body">Tell us a bit about you *</label>
                                <textarea id="body" class="form-control" placeholder="" rows="4" name="description">
                                    {{old('description')}}
                                </textarea>
                                @error("description")
                                <p class="text-danger font-weight-bold">{{$errors->first("description")}}</p>                                    
                               @enderror
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-lg-12 my-3">
                                <button class="btn btn__primary"> <i class="fa fa-envelope mr-4"></i>APPLY
                                    NOW</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section> --}}
    @endsection
    @section("js")
    @if($errors->count()>0)
    <script>
    window.location.href = "{{url()->current()}}#contactform";
    </script>
    @endif
    @endsection
