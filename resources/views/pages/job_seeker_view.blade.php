@extends('layouts.app')
@section('title')
{{ $data->title }}
@endsection
@section('content')
<section class="page__header events__header py-5 position-relative">
        <div class="container py-3">
            <div class="row">
                <div class="col-lg-8">
                    <p class="text-white sub mb-2 mt-5">Job Seeker</p>
                    <h1 class="text-white mb-4">YBLN Job Seeker</h1>
                    <p class="text-white mb-5">Dummy content is here</p>
                </div>
            </div>

        </div>
        <img src="{{ asset('assets/images/page_abstract.svg')}}" alt="" class="page_abstract">
    </section>


    <section class="event_page py-5">
        <div class="container mt-5">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="title mb-5">{{$data->title}}</h1>
                    @if($data->document!="" && $data->document!=null)
                        <a class="btn btn__primary text-uppercase my-2" href="{{ $data->document }}" download="{{ $data->title }}">Download</a>
                    @endif
                    <div class="mb-5">
                        <img src="{{ $data->image }}" class="img-fluid image" alt="">
                        <div class="text-center">
                            <!-- <p class="image_caption text-center mt-2">This is a caption on this photo for reference</p> -->
                        </div>
                    </div>
                    {!! $data->description !!}
                    <div class="row justify-content-center">
                        <div class="col-lg-9 my-5 text-center">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection