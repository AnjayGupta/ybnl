@extends('layouts.app')
@section('title','Events')
@section('content')
<section class="page__header events__header py-5 position-relative">
        <div class="container py-3">
            <div class="row">
                <div class="col-lg-8">
                    <p class="text-white sub mb-2 mt-5">ATTEND ONE OF OUR EVENTS</p>
                    <h1 class="text-white mb-4">YBLN Events</h1>
                    <p class="text-white mb-5">Upcoming events for you to attend</p>
                </div>
            </div>

        </div>
        <img src="{{ asset('assets/images/page_abstract.svg')}}" alt="" class="page_abstract">
    </section>

    <!-- blog area -->
    <section class="blog_area py-5">
        <div class="container">
            <div class="col-12">
                <div class="filter">
                    <div class="form-group position-relative">
                        <input type="text" class="form-control search-box" id="search" placeholder="Search">
                        <i class="fa fa-search"></i>
                      </div>
                </div>
            </div>
            <div class="row box">
                
                @foreach($data as $val)
                <div class="col-lg-4 my-3">
                    <div class="event__card h-100">
                        <div class="event-img">
                            <img src="{{ $val->image }}" alt="" class="img-fluid">
                        </div>
                        <div class="p-4">
                            <a href="{{route('events.deatils',$val->slug )}}">
                                <h2 class="event__title mb-3">{{$val->title}}</h2>
                            </a>
                            <p class="event__excerpts">
                                {!!\Str::limit($val->desc,300,' <a href="'.route('events.deatils',$val->slug ).'" style="margin-right:10px;text-decoration:none;font-weight:bold">... read more</a>')!!}
                                </p>
                            <ul class="list-inline">
                                <li class="">
                                    {{ $val->created_at->format('M d Y')}} | Day
                                    <!-- Mar 23 2022 | 1 Day -->
                                </li>
                            </ul>  
                        </div>
                    </div>
                </div>
                @endforeach
                <div class="col-sm-12">
                    {{$data->links()}}
                </div>
            </div>

        </div>

    </section>

@endsection

@section("js")
<script>
    
$(".search-box").on("input",function(){
     
    var ajaxCall=ajax("{{url()->current()}}",{search:$(this).val().trim()});
    showLoader();
    ajaxCall.done(function(response){
           $(".box").html(response.result);
           closeLoader();
      });
       ajaxCall.fail(function(response){
       console.log(response);
      });

});
   
</script>
@endsection