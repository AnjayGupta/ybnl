@extends('layouts.app')
@section('title')
{{$data->slug}}
@endsection
@section('content')
<section class="page__header events__header py-5 position-relative">
        <div class="container py-3">
            <div class="row">
                <div class="col-lg-8">
                    <p class="text-white sub mb-2 mt-5">ATTEND ONE OF OUR Programmes</p>
                    <h1 class="text-white mb-4">YBLN Programmes</h1>
                    <p class="text-white mb-5">Upcoming programmes for you to attend</p>
                </div>
            </div>

        </div>
        <img src="{{ asset('assets/images/page_abstract.svg')}}" alt="" class="page_abstract">
    </section>


    <section class="event_page py-5">
        <div class="container mt-5">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="title mb-5">{{$data->title}}</h1>
                    <div class="mb-5">
                        <img src="{{ $data->image }}" class="img-fluid image" alt="" style="width: 100%; height: 400px; object-fit: cover;">
                        <div class="text-center">
                            <!-- <p class="image_caption text-center mt-2">This is a caption on this photo for reference</p> -->
                        </div>
                    </div>
                    {!! $data->desc !!}
                    <div class="row mt-5">
                        <div class="col-lg-6">
                            <h5>Eligibility Criteria</h5><hr />
                            {!! $data->eligibility_criteria !!}
                        </div>
                        <div class="col-lg-6">
                            <h5>Application Process</h5><hr />
                            {!! $data->process !!}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <h5>Gallery</h5><hr />
                            @if(count($data->gallery)>0)
                                <div class="col-lg-12" style="padding: 0;">
                                    @foreach($data->gallery as $image)
                                        <img src="{{ $image->path }}" style="height: 200px;" />
                                    @endforeach
                                </div>
                            @else
                                <p>No image found!</p>
                            @endif
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-lg-9 my-5 text-center">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection