@extends('layouts.app')
@section('title')
{{ $page->title }}
@endsection
@section('content')
<section class="page__header py-5 position-relative" style="background-color: {{ $page->cover_color }};">
        <div class="container py-3">
            <div class="row">
                <div class="col-lg-8">
                    <p class="text-white sub mb-2 mt-5">{{ $page->short_description }}</p>
                    <h1 class="text-white mb-4">{{ $page->title }}</h1>
                    <p class="text-white mb-5">{{ $page->long_description}}</p>
                </div>
            </div>

        </div>
        <img src="{{ $page->cover_image }}" alt="" class="page_abstract">
    </section>


    <section class="event_page py-5">
        <div class="container mt-5">
            <div class="row">
                
                <div class="col-lg-12">
                   {!! $page->content !!}
                </div>
            </div>
        </div>
    </section>

@endsection