@foreach ($data as $list )
<div class="col-lg-6 my-3">
    <div class="blog__card h-100">
        <img src="{{ $list->image}}" alt="100 Women in Agriculture" class="img-fluid">
        <div class="p-4">
            <a href="{{route('programmes.details',$list->slug)}}" class="nav-item">
                <h2 class="blog__title mb-3">{{$list->title}}</h2>
            </a>
            <p class="blog__excerpts">{!! Str::limit($list->desc, 300, '') !!} </p>

            <button class="btn btn__eminence text-uppercase my-2">Join PROGRAM</button>
            @if($list->docs!="" && $list->docs!=null)
            <a class="btn btn__primary text-uppercase my-2" href="{{$list->docs}}" download="{{$list->title}}">View BROCHURE</a>
            @else
            <a class="btn btn__primary text-uppercase my-2" href="#">View BROCHURE</a>
            @endif
            

        </div>
    </div>
</div>                    
@endforeach



</div>

<div class="row my-4">
<div class="col-sm-12">
  <center>  {{$data->links()}}</center>
</div>
</div>