@if(count($data)>0)
@foreach($data as $val)
                <div class="col-lg-12 my-2">
                    <a href="{{ route('job-details',$val->slug)}}">
                        <div class="job_card px-4 py-3 position-relative">
                            <div class="row">
                                <div class="col-lg-6">
                                    <p class="title mb-0">
                                       {{$val->title}}
                                    </p>
                                    <p class="location  mb-0"> {{$val->location}}</p>
                                </div>
                                <div class="col-lg-6">
                                    <p class="department"> {{$val->sub_title}}</p>
                                </div>
                            </div>

                            <span class="position-absolute marker">
                                <svg width="9" height="12" viewBox="0 0 9 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M7.875 6.84766C8.19141 6.53125 8.19141 6.00391 7.875 5.65234L3.09375 0.871094C2.74219 0.554688 2.21484 0.554688 1.89844 0.871094L1.08984 1.67969C0.773438 2.03125 0.773438 2.55859 1.08984 2.875L4.5 6.28516L1.08984 9.66016C0.773438 9.97656 0.773438 10.5039 1.08984 10.8555L1.89844 11.6289C2.21484 11.9805 2.74219 11.9805 3.09375 11.6289L7.875 6.84766Z" fill="#66B22E" />
                                </svg>
                            </span>
                        </div>
                    </a>
                </div>
                @endforeach
                <div class="col-sm-12 mt-4">
                    {{$data->links()}}
                </div>
@else
<div class="col-sm-12">
    <center><h3>No any job found</h3></center>
</div>
@endif