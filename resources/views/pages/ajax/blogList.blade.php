@foreach ($data as  $item)
<div class="col-lg-4 my-3">
    <div class="blog__card h-100">
        <img src="{{$item->image}}" alt="" class="img-fluid">
        <div class="p-4">
            <a href="{{route('blog-details',$item->slug)}}" class="text-decoration-none">
                <h2 class="blog__title mb-3">{{$item->title}}</h2>
            </a>
            <p class="blog__excerpts">{!!\Str::limit($item->description,300,' <a href="'.route('blog-details',$item->slug).'" style="margin-right:10px;text-decoration:none;font-weight:bold">... read more</a>')!!} </p>

            <ul class="list-inline">
                <li class="">
                    <a href="#">Entrepreneurship</a>
                </li>
                <li class="text-right">{{$item->created_at->format("M d Y")}}</li>
            </ul>
        </div>
    </div>
</div>
    
@endforeach

<div class="col-sm-12 mt-4">
    {{$data->links()}}
</div>