@foreach($data as $val)
                <div class="col-lg-4 my-3">
                    <div class="event__card h-100">
                        <img src="{{ $val->image }}" alt="" class="img-fluid">
                        <div class="p-4">
                            <a href="{{route('events.deatils',$val->slug )}}">
                                <h2 class="event__title mb-3">{{$val->title}}</h2>
                            </a>
                            <p class="event__excerpts">
                                {!!\Str::limit($val->desc,300,' <a href="'.route('events.deatils',$val->slug ).'" style="margin-right:10px;text-decoration:none;font-weight:bold">... read more</a>')!!}
                                </p>
                            <ul class="list-inline">
                                <li class="">
                                    {{ $val->created_at->format('M d Y')}} | Day
                                    <!-- Mar 23 2022 | 1 Day -->
                                </li>
                            </ul>  
                        </div>
                    </div>
                </div>
                @endforeach
                <div class="col-sm-12 mt-4">
                    {{$data->links()}}
                </div>