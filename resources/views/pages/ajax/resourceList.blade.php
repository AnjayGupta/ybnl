@foreach ($data as $list )
           
                <div class="col-lg-12 my-2">

                    <div class="job_card px-4 py-3 position-relative">
                       
                       <div class="row align-items-center">
                        <div class="col-lg-6">
                            <p class="title mb-0">
                                {{$list->title}}
                            </p>
                            <p class="location  mb-0">{!! Str::limit($list->desc, 300, '') !!}</p>
                        </div>
                        <div class="col-lg-6 text-right">
                            <a class="btn btn__primary" href="{{$list->docs}}" download=" {{$list->title}}">Download</a>
                        </div>
                    </div>
                           
                      

                    </div>

                </div>

            
            @endforeach
            <div class="row">
                <div class="col-sm-12">
                    {{$data->links()}}

                </div>
            </div>