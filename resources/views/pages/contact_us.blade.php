@extends('layouts.app')
@section('title','Job markets')
@section('content')
<section class="page__header contact__header py-5 position-relative">
        <div class="container py-3">
            <div class="row">
                <div class="col-lg-8">
                    <p class="text-white sub mb-2 mt-5">Contact Us</p>
                    <h1 class="text-white mb-4">Get In Touch</h1>
                    <p class="text-white mb-5">We would love to hear from you either by mail, phone call or at our
                        office in the heart of the enterpreneur part of Lagos, Nigeria.</p>
                </div>
            </div>

        </div>
        <img src="{{ asset('assets/images/page_abstract.svg')}}" alt="" class="page_abstract">
    </section>


    <section class="py-5 contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mt-5">
                    <p class="section__sub mb-2">What are you waiting for</p>
                    <h2 class="primary__text section__title font-weight-bold mb-3">Contact Us</h2>
                    <p>Feel Free to use the contact details below, alternatively kindly fill in the form;</p>
                </div>
            </div>

            <div class="row justify-content-center">
                <div class="col-lg-10">

                    <form class="my-5">
                        <div class="form-row">
                            <div class="col-lg-6 my-3">
                                <label for="fullname">Full Name *</label>
                                <input type="text" id="fullname" class="form-control" placeholder="Full name" autocomplete="name">
                            </div>
                            <div class="col-lg-6 my-3">
                                <label for="email">Contact Email *</label>
                                <input type="email" id="email" class="form-control" placeholder="Contact email" autocomplete="email">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-lg-12 my-3">
                                <label for="body">Your message*</label>
                                <textarea id="body" class="form-control" placeholder="" rows="4"></textarea>
                            </div>
                        </div>
                        <p>By submitting this form you agree to our terms and conditions and our Privacy Policy which
                            explains how we may collect, use and disclose your personal information including to third
                            parties.</p>

                        <div class="form-row">
                            <div class="col-lg-12 my-3">
                                <button class="btn btn__primary"> Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-4 my-3 text-center">
                    <div class="contact_info">
                        <img src="{{ asset('assets/images/Icon_Email.svg')}}" alt="" class="">

                        <h5 class="my-4">Email us</h5>
                        <p>Email us for general queries, including marketing and partnership opportunities.</p>
                        <a href="mailto:{{ $adminSettings->email }}">{{ $adminSettings->email }}</a>
                    </div>
                </div>

                <div class="col-lg-4 my-3 text-center">
                    <div class="contact_info">
                        <img src="{{ asset('assets/images/Icon_Call.svg')}}" alt="" class="">

                        <h5 class="my-4">Call us</h5>
                        <p>Call us to speak to a member of our team. We are always happy to help.</p>
                        <a href="tel:{{ $adminSettings->phone }}">{{ $adminSettings->phone }}</a>
                    </div>
                </div>

                <div class="col-lg-4 my-3 text-center">
                    <div class="contact_info">
                        <img src="{{ asset('assets/images/road-map-fill.svg')}}" alt="" class="">

                        <h5 class="my-4">Address</h5>
                        <p>{{ $adminSettings->address }}</p>
                        <a href="#">{{ $adminSettings->timing }}</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection