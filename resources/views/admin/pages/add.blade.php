@extends("layouts.admin_layout")
@section("title","Pages")
@section("content")
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">Pages</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="#">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">Add Page</a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                   
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <h4 class="card-title">Add New Page</h4>
                            <a href="{{route('admin.page.index')}}" class="btn btn-primary">Pages List</a>
                            
                        </div>
                        <div class="card-body">
                            <form action="{{route('admin.page.store')}}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label for="">Title</label>
                                    <input type="text" name="title" id="" class="form-control" placeholder="Title" value="{{old('title')}}">
                                </div>
                                <div class="form-group">
                                    <label for="">Short Description</label>
                                    <input type="text" name="short_description" id="" class="form-control" placeholder="Short Description" value="{{old('short_description')}}">
                                </div>
                                <div class="form-group">
                                    <label for="">Long Description</label>
                                    <input type="text" name="long_description" id="" class="form-control" placeholder="Long Description" value="{{old('long_description')}}">
                                </div>
                                <div class="form-group">
                                    <label for="">Cover Color (e.g., #ffffff)</label>
                                    <input type="text" name="cover_color" id="" class="form-control" placeholder="Cover Color" value="{{old('cover_color')}}">
                                </div>
                                <div class="form-group">
                                    <label for="">Select Cover Image</label>
                                    <input type="file" name="cover_image" id="" class="form-control" placeholder="Title">
                                </div>
                                <div class="form-group">
                                    <label for="">Content</label>
                                    <textarea name="content" id="editor">
                                        {{old('content')}}
                                    </textarea>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary">Add Page</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                
               
               
            </div>
        </div>
    </div>
   
</div>
@endsection
@section("js")
    <script src="https://cdn.ckeditor.com/4.7.0/full/ckeditor.js"></script>
    <script>CKEDITOR.replace( 'editor');</script>
@endsection