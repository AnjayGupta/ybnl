@extends("layouts.admin_layout")
@section("title","Dashboard")
@section("content")
<div class="main-panel">
			<div class="content">
				<div class="panel-header bg-primary-gradient">
					<div class="page-inner py-5">
						<div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
							<div>
								<h2 class="text-white pb-2 fw-bold">Dashboard</h2>
								<h5 class="text-white op-7 mb-2">All Activity List And Details</h5>
							</div>
							
						</div>
					</div>
				</div>
				<div class="page-inner mt--5">
					<div class="row mt--2">
						<div class="col-md-3">
							<div class="card full-height text-white bg-primary-gradient ">
								<div class="card-body">
									<div class="card-title text-center font-weight-bold text-white">Total Programmers</div>
									<center><h3>{{$program}}</h3></center>
									
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="card full-height bg-warning text-white">
								<div class="card-body">
									<div class="card-title text-center font-weight-bold text-white">Total Events</div>
									<center><h3>{{$event}}</h3></center>
									
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="card full-height bg-info-gradient text-white">
								<div class="card-body">
									<div class="card-title text-center font-weight-bold text-white">Total Jobs</div>
									<center><h3>{{$job}}</h3></center>
									
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="card full-height bg-success-gradient text-white">
								<div class="card-body">
									<div class="card-title text-center font-weight-bold text-white">Total Applied Jobs</div>
									<center><h3>{{$applied_job}}</h3></center>
									
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</div>
			
		</div>
@endsection