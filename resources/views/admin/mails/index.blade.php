@extends("layouts.admin_layout")
@section("content")
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">Mails</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="#">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">Mails create and list</a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                   
                </ul>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-body p-0">
                            <table id="recentCMEAdded" class="bg-white table-striped table text-center">
                                <thead >
                                    <tr class="bg-success text-white">
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Action</th>
                                       
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data as $list)
                                     <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{$list->name}}</td>
                                        <td>{{$list->email}}</td>
                                        <td><a href="javascript:void(0)" action="{{route('admin.mail.delete',encode($list->id))}}" class="delete-btn"><i class="bi bi-trash btn btn-danger px-2 py-1" style="font-size:10px;margin-right:5px;"></i></a></td>
                                     </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <h3><b>Add Mails</b></h3>
                            <hr>
                            <form action="{{route('admin.mail.store')}}" method="POST" enctype="multipart/form-data"> 
                                @csrf
                                <div class="form-group">
                                    <label for="">Name</label>
                                    <input type="text" name="name" id="" class="form-control" value="{{old('name')}}" placeholder="Name">
                                    @error("name")
                                <p class="text-danger">{{$errors->first("name")}}</p>
                                        
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="">Email</label>
                                    <input type="text" name="email" id="" class="form-control" value="{{old('email')}}" placeholder="Email">
                                    @error("email")
                                    <p class="text-danger">{{$errors->first("email")}}</p>
                                            
                                        @enderror
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Add Mails</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                
               
               
            </div>
        </div>
    </div>
   
</div>
@endsection
@section("js")
<script>
   
   $(".delete-btn").click(function(e){
    e.preventDefault();
    var url=$(this).attr("action");
    Swal.fire({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.isConfirmed) {
    window.location.href=url;
  }
})

   });
</script>
@endsection
