
@extends("layouts.admin_layout")
@section("title","Add Event")
@section("content")
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">Events</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="#">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">Add New Events</a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                   
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <h4 class="card-title">Add New Events</h4>
                            <a href="{{route('admin.event.index')}}" class="btn btn-primary">Events List</a>
                            
                        </div>
                        <div class="card-body">
                            <form action="{{ route('admin.store.event')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label for="">Title</label>
                                    <input type="text" placeholder="Title" name="title" class="form-control ">
                                </div>
                                <div class="form-group">
                                    <label for="">Image</label>
                                    <input type="file" placeholder="Sub Title" name="image" class="form-control ">
                                </div>
                                

                                <div class="form-group">
                                    <label for="">Description</label>
                                    <textarea name="description" id="editor" cols="30" rows="10" ></textarea>
                                </div>

                                <div class="form-group">
                                    <button class="btn btn-primary" type="submit">Add New Events</button>
                                </div>
                           
                            </form>
                        </div>
                    </div>
                </div>
                
               
               
            </div>
        </div>
    </div>
   
</div>
@endsection
@section("js")
<script src="https://cdn.ckeditor.com/4.7.0/full/ckeditor.js"></script>
<script>CKEDITOR.replace( 'editor');</script>
@endsection