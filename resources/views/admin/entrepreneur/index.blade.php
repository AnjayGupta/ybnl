@extends("layouts.admin_layout")
@section("title", "Entrepreneurs List")
@section("content")
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">Entrepreneurs</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="#">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">Entrepreneurs List</a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                   
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between justify-items-center">
                            <h4 class="card-title">Entrepreneurs List</h4>
                            <a href="{{route('admin.entrepreneur.add')}}" class="btn btn-primary">Add New Entrepreneur</a>
                            
                        </div>
                        <div class="card-body">
                            <table id="recentCMEAdded" class="bg-white table-striped table">
                                <thead >
                                    <tr class="bg-success text-white">
                                        <th>#</th>
                                        <th>Image</th>
                                        <th>Title</th>
                                        <th>Document</th>
                                        <th>Description</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data as $val)
                                    <tr>
                                        <td width="5%">{{ $loop->iteration }}</td>
                                        <td width="15%">
                                            <img src="{{ $val->image }}" alt="" class="img-fluid me-2" width="50">
                                        </td>
                                        <td>{{ $val->title }}</td>
                                        <td width="15%">
                                            @if($val->document)
                                                <a href="{{ $val->document }}" download="{{ $val->title }}" target="_blank" class="btn btn-primary px-2 py-1">Download</a>
                                            @else
                                                N/A
                                            @endif
                                        </td>
                                        <td width="40%">{!! \Illuminate\Support\Str::limit($val->description, 150, ' ...') !!}</td>
                                        
                                            <td class="d-flex justify-content-between align-items-center">
                                                <a href="{{ route('admin.entrepreneur.edit', $val->id) }}">
                                                    <i class="bi bi-pencil-square btn btn-success px-2 py-1" style="font-size:10px;margin-right:5px;"></i>
                                                </a>
                                                <a href="javascript:void(0)" action="{{ route('admin.entrepreneur.delete', $val->id) }}" class="delete-btn">
                                                    <i class="bi bi-trash btn btn-danger px-2 py-1" style="font-size:10px;"></i>
                                                </a>
                                            </td>
                                            
                                       
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                
               
               
            </div>
        </div>
    </div>
   
</div>
@endsection
@section("js")
<script>
   
   $(".delete-btn").click(function(e){
    e.preventDefault();
    var url=$(this).attr("action");
    Swal.fire({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.isConfirmed) {
    window.location.href=url;
  }
})

   });
</script>
@endsection
