@extends("layouts.admin_layout")
@section("title","Add Profile")
@section("content")
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">Profiles</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="#">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">Add Profile</a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                   
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <h4 class="card-title">Add New Profile</h4>
                            <a href="{{route('admin.profiles')}}" class="btn btn-primary">Profiles List</a>
                            
                        </div>
                        <div class="card-body">
                            <form action="{{route('admin.profile.store')}}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label for="">Title</label>
                                    <input type="text" name="title" id="" class="form-control" placeholder="Title">
                                </div>
                                <div class="form-group">
                                    <label for="">Description</label>
                                    <input type="text" name="description" id="" class="form-control" placeholder="Description">
                                </div>
                                <div class="form-group">
                                    <label for="">Overlay Color (e.g, #fcffb767)</label>
                                    <input type="text" name="overlay_color" id="" class="form-control" placeholder="Overlay color">
                                </div>
                                <div class="form-group">
                                    <label for="">Select Image</label>
                                    <input type="file" name="image" id="" class="form-control">
                                </div>

                                <div class="form-group">
                                    <button class="btn btn-primary">Add New Profile</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                
               
               
            </div>
        </div>
    </div>
   
</div>
@endsection
