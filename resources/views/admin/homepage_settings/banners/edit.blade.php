@extends("layouts.admin_layout")
@section("title","Edit Banner")
@section("content")
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">Banners</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="#">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">Edit Banner</a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                   
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <h4 class="card-title">Update Banner</h4>
                            <a href="{{route('admin.banners')}}" class="btn btn-primary">Banners List</a>
                            
                        </div>
                        <div class="card-body">
                            <form action="{{ route('admin.banner.update')}}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" id="" value="{{ $data->id }}">
                                <div class="form-group">
                                    <label for="">Title</label>
                                    <input type="text" name="title" id="" class="form-control" placeholder="Title" value="{{ $data->title }}">
                                </div>
                                <div class="form-group">
                                    <label for="">Short Description</label>
                                    <input type="text" name="short_description" id="" class="form-control" placeholder="Short description" value="{{ $data->short_description }}">
                                </div>
                                <div class="form-group">
                                    <label for="">Long Description</label>
                                    <textarea name="long_description" placeholder="Long description" class="form-control">{{ $data->long_description }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="">Select Image</label>
                                    <input type="file" name="cover_image" id="" class="form-control" value="">
                                    <img id="output" style="width:210px;" src="{{ $data->cover_image }}" />
                                </div>

                                <div class="form-group">
                                    <button class="btn btn-primary">Update Banner</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                
               
               
            </div>
        </div>
    </div>
   
</div>
@endsection
