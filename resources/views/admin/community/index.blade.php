@extends("layouts.admin_layout")
@section("title","Community")
@section("content")
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">Community</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="#">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">Community list</a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                   
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                           <table class="table">
                            <tr>
                                <th>Sr.No</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Action</th>
                            </tr>
                            @foreach ($data as $list )
                               <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$list->first_name.' '.$list->last_name}}</td>
                                <td>{{$list->email}}</td>
                                <td>{{$list->phone}}</td>
                                <td class="d-flex justify-content-between align-items-center align-content-center" width="5%">
                                <a href="javascript:void(0)" action="{{route('admin.community.delete',encode($list->id))}}" class="delete-btn"><i class="bi bi-trash btn btn-danger px-2 py-1" style="font-size:10px;"></i></a>
                                </td>
                               </tr>
                                
                            @endforeach
                           </table>
                        </div>
                    </div>
                </div>
                
               
               
            </div>
        </div>
    </div>
   
</div>
@endsection

@section("js")
<script>
   
   $(".delete-btn").click(function(e){
    e.preventDefault();
    var url=$(this).attr("action");
    Swal.fire({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.isConfirmed) {
    window.location.href=url;
  }
})

   });
</script>
@endsection