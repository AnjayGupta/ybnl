@extends("layouts.admin_layout")
@section("title","Add programmers")
@section("content")
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">Programmes</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="#">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">Add Programmes</a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                   
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <h4 class="card-title">Add New Programmes</h4>
                            <a href="{{route('admin.programmers')}}" class="btn btn-primary">Programmer List</a>
                            
                        </div>
                        <div class="card-body">
                            <form action="{{route('admin.store.programmer')}}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label for="">Title</label>
                                    <input type="text" name="title" id="" class="form-control" placeholder="Title">
                                </div>
                                <div class="form-group">
                                    <label for="">Select Image</label>
                                    <input type="file" name="image" id="" class="form-control" placeholder="Title">
                                </div>
                                <div class="form-group">
                                    <label for="">Select Gallery Images</label>
                                    <input type="file" name="images[]" id="" class="form-control" multiple>
                                </div>
                                <div class="form-group">
                                    <label for="">Upload Brochure</label>
                                    <input type="file" name="docs" id="" class="form-control" placeholder="Title" accept=".pdf">
                                </div>

                                <div class="form-group">
                                    <label for="">Description</label>
                                    <textarea name="description" id="editor"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="">Eligibility Criteria</label>
                                    <textarea name="eligibility_criteria" id="eligibility_criteria"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="">Application Process</label>
                                    <textarea name="process" id="process"></textarea>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary">Add New Programmers</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                
               
               
            </div>
        </div>
    </div>
   
</div>
@endsection
@section("js")
<script src="https://cdn.ckeditor.com/4.7.0/full/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'editor');
    CKEDITOR.replace( 'eligibility_criteria');
    CKEDITOR.replace( 'process');
</script>
@endsection