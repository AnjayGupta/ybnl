@extends("layouts.admin_layout")
@section("title","Team Members")
@section("content")
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">Team Members</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="#">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">Team Members</a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                   
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <h2 class="card-title">Team Members</h2>
                            <a class="btn btn-primary" href="{{route('admin.team_member.add')}}">Add New Team Member</a>
                            
                        </div>
                        <div class="card-body">
                            <table id="recentCMEAdded" class="bg-white text-center table-striped table">
                                <thead>
                                    <tr class="bg-success text-white">
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Image</th>
                                        <th>Designation</th>
                                        <th>Type</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($data)>0)
                                    @foreach($data as $val)
                                    <tr>
                                        <td width="5%">{{$loop->iteration }}</td>
                                        <td width="15%">{{$val->name }}</td>
                                        <td width="20%">
                                            @if($val->image)
                                                <img src="{{$val->image}}" alt="" width="50">
                                            @else
                                                <p>N/A</p>
                                            @endif
                                        </td>
                                        <td width="45%">{{ $val->designation }}</td>
                                        <td width="45%">
                                            @if($val->type == "advisory-board")
                                                Advisory Board
                                            @endif
                                            @if($val->type == "board-member")
                                                Board Member
                                            @endif
                                        </td>
                                        <td class="d-flex justify-content-between align-items-center" width="5%">
                                            <a href="{{route('admin.team_member.edit',encode($val->id)) }}"><i class="bi bi-pencil-square btn btn-success px-2 py-1" style="font-size:10px;margin-right:5px;"></i></a>
                                            <a href="javascript:void(0)" action="{{route('admin.team_member.delete',encode($val->id))}}" class="delete-btn"><i class="bi bi-trash btn btn-danger px-2 py-1" style="font-size:10px;"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                        <td colspan="5"><h3>No data found</h3></td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                            {{$data->links()}}
                        </div>
                    </div>
                </div>
                
               
               
            </div>
        </div>
    </div>
   
</div>
@endsection
@section("js")
<script>
   
   $(".delete-btn").click(function(e){
    e.preventDefault();
    var url=$(this).attr("action");
    Swal.fire({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.isConfirmed) {
    window.location.href=url;
  }
})

   });
</script>
@endsection