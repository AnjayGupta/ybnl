@extends("layouts.admin_layout")
@section("title","Team Member")
@section("content")
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">Team Member</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="#">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">Add Team Member</a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                   
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <h4 class="card-title">Add New Team Member</h4>
                            <a href="{{route('admin.team_member.index')}}" class="btn btn-primary">Team Member List</a>
                            
                        </div>
                        <div class="card-body">
                            <form action="{{route('admin.team_member.store')}}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label for="">Name</label>
                                    <input type="text" name="name" id="" class="form-control" placeholder="Name" value="{{old('name')}}">
                                </div>
                                <div class="form-group">
                                    <label for="">Designation</label>
                                    <input type="text" name="designation" id="" class="form-control" placeholder="Designation" value="{{old('designation')}}">
                                </div>
                                <div class="form-group">
                                    <label for="">Type</label>
                                    <select name="type" class="form-control">
                                        <option value="advisory-board" selected>Advisory Board</option>
                                        <option value="board-member">Board Member</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Select Image</label>
                                    <input type="file" name="image" id="" class="form-control" placeholder="Title">
                                </div>

                                <div class="form-group">
                                    <button class="btn btn-primary">Add Team Member</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                
               
               
            </div>
        </div>
    </div>
   
</div>
@endsection