@extends("layouts.admin_layout")
@section("title","Add Job Seeker")
@section("content")
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">Job Seekers</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="#">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">Add job Seeker</a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                   
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <h4 class="card-title">Add New Job Seeker</h4>
                            <a href="{{route('admin.job_seeker.index')}}" class="btn btn-primary">Job Seekers List</a>
                            
                        </div>
                        <div class="card-body">
                            <form action="{{route('admin.job_seeker.store')}}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label for="">Title</label>
                                    <input type="text" name="title" id="" class="form-control" placeholder="Title">
                                </div>
                                <div class="form-group">
                                    <label for="">Select Image</label>
                                    <input type="file" name="image" id="" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="">Upload Document</label>
                                    <input type="file" name="document" id="" class="form-control" accept=".pdf">
                                </div>

                                <div class="form-group">
                                    <label for="">Description</label>
                                    <textarea name="description" id="editor"></textarea>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary">Add New Job Seeker</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                
               
               
            </div>
        </div>
    </div>
   
</div>
@endsection

@section("js")
    <script src="https://cdn.ckeditor.com/4.7.0/full/ckeditor.js"></script>
    <script>CKEDITOR.replace( 'editor');</script>
@endsection