@extends("layouts.admin_layout")
@section("title","Edit Job Seeker")
@section("content")
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">Job Seekers</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="#">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">Edit Job Seeker</a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                   
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <h4 class="card-title">Update Job Seeker</h4>
                            <a href="{{route('admin.job_seeker.index')}}" class="btn btn-primary">Job Seekers List</a>
                            
                        </div>
                        <div class="card-body">
                            <form action="{{ route('admin.job_seeker.update')}}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" id="" value="{{ $data->id }}">
                                <div class="form-group">
                                    <label for="">Title</label>
                                    <input type="text" name="title" id="" class="form-control" placeholder="Title" value="{{ $data->title }}">
                                </div>
                                <div class="form-group">
                                    <label for="">Select Image</label>
                                    <input type="file" name="image" id="" class="form-control" value="">
                                    <img id="output" style="width:210px;" src="{{ $data->image }}" />
                                </div>

                                <div class="form-group">
                                    <label for="">Upload Document</label>
                                    <input type="file" name="document" id="" class="form-control" accept=".pdf">
                                    @if($data->document)
                                        <iframe src="{{ $data->document }}" frameborder="0"></iframe>
                                    @endif
                                   
                                </div>

                                <div class="form-group">
                                    <label for="">Description</label>
                                    <textarea name="description" id="editor">
                                        {{ $data->description }}
                                    </textarea>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary">Update Job Seeker</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                
               
               
            </div>
        </div>
    </div>
   
</div>
@endsection

@section("js")
    <script src="https://cdn.ckeditor.com/4.7.0/full/ckeditor.js"></script>
    <script>CKEDITOR.replace( 'editor');</script>
@endsection