

@extends("layouts.admin_layout")
@section("content")
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">Settings</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="#">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">Update Settings</a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                   
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <h4 class="card-title">Update Settings</h4>
                            
                        </div>
                        <div class="card-body">
                            <form action="{{ route('admin.setting.update')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                   
                                    
                                    <div class="col"> <div class="form-group">
                                        <label for="">Email</label>
                                        <input type="text" placeholder="Email" name="email" class="form-control " value="{{$setting->email}}">
                                    </div></div>
                                    <div class="col"><div class="form-group">
                                        <label for="">Phone</label>
                                        <input type="text" placeholder="Phone" name="phone" class="form-control " value="{{$setting->phone}}">
                                    </div></div>
                                </div>
                               <div class="row">
                                
                               
                                <div class="col"><div class="form-group">
                                    <label for="">Facebook Link</label>
                                    <input type="text" placeholder="Facebook Link " name="facebook_link" class="form-control " value="{{$setting->facebook_link}}">
                                </div></div>
                                <div class="col"> <div class="form-group">
                                    <label for="">Instagram Link</label>
                                    <input type="text" placeholder="Instagram Link " name="youtube_link" class="form-control " value="{{$setting->youtube_link}}">
                                </div></div>
                               </div>
                               <div class="row">
                                
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="">Linkedin Link</label>
                                            <input type="text" placeholder="Linkedin Link " name="linkedin" class="form-control " value="{{$setting->linkedin}}">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="">Twitter Link</label>
                                            <input type="text" placeholder="Twitter Link " name="twitter_link" class="form-control " value="{{$setting->twitter_link}}">
                                        </div>
                                    </div>
                              
                               </div>
                               <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="">Address</label>
                                            <textarea class="form-control" name="address" placeholder="Address">{{ $setting->address }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="">Timing</label>
                                            <input type="text" placeholder="Timing" name="timing" class="form-control " value="{{$setting->timing}}">
                                        </div>
                                    </div>
                               </div>
                               <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="">App Icon</label>
                                        <input type="file" placeholder="Twitter Link " name="app_icon" class="form-control ">
                                        @if($setting->app_icon)
                                        <img src="{{$setting->app_icon}}" alt="" width="100">
                                        @endif
                                    </div>
                                </div>
                               </div>

                                <div class="form-group">
                               

                                <div class="form-group">
                                    <button class="btn btn-primary" type="submit">Update Settings</button>
                                </div>
                           
                            </form>
                        </div>
                    </div>
                </div>
                
               
               
            </div>
        </div>
    </div>
   
</div>
@endsection
