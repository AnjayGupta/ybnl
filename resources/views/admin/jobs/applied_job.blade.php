@extends("layouts.admin_layout")
@section("title","Applied Job");
@section("content")
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">Applied Jobs</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="#">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">Apllied Job List</a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                   
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        
                        <div class="card-body p-0 mb-0">
                            <table id="recentCMEAdded" class="bg-white text-center table-hover table">
                                <thead>
                                    <tr class="bg-success text-white">
                                        <th>#</th>
                                        <th>Applied Post Job</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Subject</th>
                                        <th>Resume</th>
                                        <th>Description</th>
                                        <th>Action</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data as $list )
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{$list->title}}</td>
                                        <td>{{$list->name}}</td>
                                        <td>{{$list->email}}</td>
                                        <td>{{$list->subject}}</td>
                                        <td><a href="{{$list->resume}}" download="{{$list->name}} Resume" class="btn btn-outline-primary py-1 px-2">Download</a></td>
                                        <td>{{$list->desc}}</td>
                                        <td><a href="javascript:void(0)" action="{{route('admin.applied_job.delete',encode($list->id))}}" class="delete-btn"><i class="bi bi-trash btn btn-danger px-2 py-1" style="font-size:10px;margin-right:5px;"></i></a></td>
                                    </tr>
                                        
                                    @endforeach
                                </tbody>
                            </table>
                            {{$data->links()}}
                        </div>
                        <td>
                          
                            
                        </td>
                    </div>
                </div>
                
               
               
            </div>
        </div>
    </div>
   
</div>
@endsection
@section("js")
<script>
   
   $(".delete-btn").click(function(e){
    e.preventDefault();
    var url=$(this).attr("action");
    Swal.fire({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.isConfirmed) {
    window.location.href=url;
  }
})

   });
</script>
@endsection