

@extends("layouts.admin_layout")
@section("title","Add Job")
@section("content")
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">Jobs</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="#">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">Add New Jobs</a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                   
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <h4 class="card-title">Add New Jobs</h4>
                            <a href="{{route('job.index')}}" class="btn btn-primary">Jobs List</a>
                            
                        </div>
                        <div class="card-body">
                            <form action="{{ route('admin.store.job')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label for="">Title</label>
                                    <input type="text" placeholder="Title" name="title" class="form-control " value="{{old('title')}}">
                                </div>
                                <div class="form-group">
                                    <label for="">Sub Title</label>
                                    <input type="text" placeholder="Sub Title" name="sub_title" class="form-control " value="{{old('sub_title')}}">
                                </div>
                                <div class="form-group">
                                    <label for="">Location</label>
                                    <input type="text" placeholder="Location " name="location" class="form-control " value="{{old('location')}}">
                                </div>

                                <div class="form-group">
                                    <label for="">Link or Email</label>
                                    <input type="text" placeholder="Link or Email " name="link" class="form-control " value="{{old('link')}}">
                                </div>

                                <div class="form-group">
                                    <label for="">Description</label>
                                    <textarea name="description" id="editor" cols="30" rows="10" >
                                        {{old('description')}}
                                    </textarea>
                                </div>

                                <div class="form-group">
                                    <button class="btn btn-primary" type="submit">Add New Jobs</button>
                                </div>
                           
                            </form>
                        </div>
                    </div>
                </div>
                
               
               
            </div>
        </div>
    </div>
   
</div>
@endsection
@section("js")
<script src="https://cdn.ckeditor.com/4.7.0/full/ckeditor.js"></script>
<script>CKEDITOR.replace( 'editor');</script>
@endsection