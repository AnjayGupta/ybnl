<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use App\Models\Mail as MailModel;
use Mail;
use Illuminate\Queue\SerializesModels;

class SendMailAppliedJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $data;
    /**
     * Create a new job instance.
     */
    public function __construct($data)
    {
        $this->data=$data;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {

        $emails=MailModel::select("email")->get();
        $email=$emails->map(function($item){
             return $item->email;
        });
        $data=$this->data;
        Mail::send("emails.applied_job",['data'=>$data],function($message)use($email,$data){
              $message->subject($data['name']." Applied For ".$data['subject']);
              $message->attachData($data['resume'], $data['name']." Resume.pdf", [
                'mime' => 'application/pdf',
            ]);
            foreach ($email as $list) {
                $message->to($list);
            }
        });

        
    }
}
