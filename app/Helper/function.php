<?php
use Vinkla\Hashids\Facades\Hashids;
use App\Models\AdminSetting;
function decode($id){
    return Hashids::decode($id)[0];
}
function encode($id){
    return Hashids::encode($id);
}

function setting(){
    return cache()->remember("setting",60*60*24,function(){
        return AdminSetting::first();
    });

}

function activeMenu($uri = '') {
    $active = '';
    if (Request::is(Request::segment(1) . '/' . $uri . '/*') || Request::is(Request::segment(1) . '/' . $uri) || Request::is($uri)) {
        $active = 'active submenu';
    }
    return $active;
}
function active($uri = '') {
    $active = '';
    if (Request::is(Request::segment(1) . '/' . $uri . '/*') || Request::is(Request::segment(1) . '/' . $uri) || Request::is($uri)) {
        $active = 'show';
    }
    return $active;
}

function social(){
    return ' <li class="list-inline-item mr-4">
    <a href="'.setting()->facebook_link.'" target="_blank"><img src="'.asset('assets/images/facebook-share.svg').'" alt="follow on facebook" class="img-fluid"></a>
</li>
<li class="list-inline-item mr-4">
    <a href="'.setting()->youtube_link.'" target="_blank"><img src="'.asset('assets/images/instagram-share.svg').'" alt="follow on Instagram" class="img-fluid"></a>
</li>
<li class="list-inline-item mr-4">
    <a href="'.setting()->twitter_link.'" target="_blank"><img src="'.asset('assets/images/twitter-share.svg').'" alt="follow on twitter" class="img-fluid"></a>
</li>
<li class="list-inline-item">
    <a href="'.setting()->linkedin.'" target="_blank"><img src="'.asset('assets/images/linkedin-share.svg').'" alt="follow on linkedin" class="img-fluid"></a>
</li>';
}

function socialShare(){
    return ' <li class="my-4">
    <a href="'.setting()->facebook_link.'" target="_blank"><img src="'.asset('assets/images/facebook-share.svg').'" alt="follow on facebook" class="img-fluid"></a>
</li>
<li class="my-4">
    <a href="'.setting()->youtube_link.'" target="_blank"><img src="'.asset('assets/images/instagram-share.svg').'" alt="follow on Instagram" class="img-fluid"></a>
</li>
<li class="my-4">
    <a href="'.setting()->twitter_link.'" target="_blank"><img src="'.asset('assets/images/twitter-share.svg').'" alt="follow on twitter" class="img-fluid"></a>
</li>
<li class="my-4">
    <a href="'.setting()->linkedin.'" target="_blank"><img src="'.asset('assets/images/linkedin-share.svg').'" alt="follow on linkedin" class="img-fluid"></a>
</li>';
}



?>