<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use HasFactory;

    protected $fillable=['title','image','description','slug'];

    protected $hidden = ['deleted_at'];
    protected $dates = ['deleted_at'];
    public static function rule(){
        return array(
            "title"=>"required",
            "image"=>"required|mimes:jpg,jpeg,svg,png|max:2048",
            "description"=>"required",
        );
    }
}
