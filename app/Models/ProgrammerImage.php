<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Programmers;

class ProgrammerImage extends Model
{
    use HasFactory;
    protected $table = 'programmer_images';
    protected $fillable = ['programmers_id', 'path'];

    public function programmer(){
        return $this->hasMany(Programmers::class);
    }
}
