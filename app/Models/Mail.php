<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mail extends Model
{
    use HasFactory;
    public static  $rule=array(
        "name"=>"required|max:100",
        "email"=>"required|email|unique:mails,email",
        
    );

   
    public static $message=array(
        "name.required"=>"Name is required",
        "name.max"=>"Name character sholud be less than 100",
        "email.required"=>"Email is required",
        "email.email"=>"Email format is invalid",
        "email.unique"=>"Email is already exists",
       
        
    );

    protected $fillable=['name','email'];
}
