<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TeamMember extends Model
{
    use HasFactory;

    protected $fillable=['name', 'designation', 'type', 'image'];

    public static function rule(){
        return [
            "name" => "required",
            "designation" => "required",
            "type" => "required|in:advisory-board,board-member",
            "image" => "required_if:type,advisory-board|mimes:jpg,jpeg,svg,png|max:2048",
        ];
    }
}
