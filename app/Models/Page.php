<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use HasFactory;

    protected $fillable=['slug', 'title', 'short_description', 'long_description', 'content', 'cover_color', 'cover_image'];

    public static function rule(){
        return [
            "title" => "required",
            "content" => "required",
            "cover_image" => "required|mimes:jpg,jpeg,svg,png|max:2048",
        ];
    }
}
