<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
class AppliedJob extends Model
{
    use HasFactory;
   
   protected $fillable=['job_id','name','email','subject','resume','desc','status'];



    public static  $rule=array(
        "name"=>"required|max:100",
        "email"=>"required|email",
        "subject"=>"required|max:1000",
        "description"=>"required|max:2000",
        "resume"=>"required|mimes:pdf|max:5120",
    );

   
    public static $message=array(
        "name.required"=>"Name is required",
        "name.max"=>"Name character sholud be less than 100",
        "email.required"=>"Email is required",
        "email.email"=>"Email format is invalid",
        "subject.required"=>"Subject is required",
        "subject.max"=>"Subject character sholud be less than 1000",
        "description.required"=>"Description is required",
        "description.max"=>"Description character sholud be less than 2000",
        "resume.required"=>"Select Your resume",
        "resume.mimes"=>"Resume type sholud be pdf",
        "resume.max"=>"Maximum 5 MB size allowed",
        
    );
}
