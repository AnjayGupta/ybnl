<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\ProgrammerImage;

class Programmers extends Model
{
    use SoftDeletes;
    protected $table = 'programmers';
    protected $fillable = ['title','desc','image','btn1','btn2','slug','docs', 'eligibility_criteria', 'process'];

    public function gallery(){
        return $this->hasMany(ProgrammerImage::class);
    }

}
