<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{
    use HasFactory;


     protected $fillable=['title','docs','desc'];
    public static  $rule=array(
        "title"=>"required|max:100",
        "docs"=>"required|mimes:pdf|max:5120",
        "desc"=>"required|max:2000",
    
    );

    public static function updateRule(){
        $rule=array(
        "title"=>"required|max:100",
        "desc"=>"required|max:2000"
    );
    if(request()->hasFile("docs")){
        $rule['docs']="mimes:pdf|max:5120";
    }
    return $rule;
    }

   
    public static $message=array(
        "title.required"=>"Title is required",
        "title.max"=>"Title character sholud be less than 100",
        "description.required"=>"Description is required",
        "description.max"=>"Description character sholud be less than 2000",
    );
}
