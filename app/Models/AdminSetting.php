<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminSetting extends Model
{
    use HasFactory;
    protected $fillable=['email','phone','facebook_link','twitter_link','youtube_link','linkedin','app_icon', 'address', 'timing'];

    public static function boot(){
        parent::boot();
        self::updating(function($model){
          cache()->forget("setting");
        });        
    }
}
