<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HomepageProfile extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'description', 'overlay_color', 'image', 'active'];
}
