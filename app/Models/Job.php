<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Vinkla\Hashids\Facades\Hashids;
class Job extends Model
{
    use SoftDeletes;
    protected $table = 'job';
    protected $fillable = ['title','desc','image','link','location'];
    protected $appends = ['hashid'];
   
    public function getHashidAttribute()
{
    return Hashids::encode($this->attributes['id']);
}
}
