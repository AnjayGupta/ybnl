<?php
namespace App\Traits;
use Illuminate\Support\Str;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Filesystem\Filesystem;
trait UploadTraits{

    public function UploadFile( $path, $test = true ) {
        $filesystem = new Filesystem;
        $name = $filesystem->name( $path );
        $extension = $filesystem->extension( $path );
        $originalName = $name . '.' . $extension;
        $mimeType = $filesystem->mimeType( $path );
        $error = null;
        return new UploadedFile( $path, $originalName, $mimeType, $error, $test );
    }

    public function uploadStorage($file,$path){
         $name=Str::random(20).".".$file->extension();
        if(Storage::disk("public")->exists($path)){
            
            $file=Storage::disk("public")->put($path."/".$name,file_get_contents($file));
            return request()->getSchemeAndHttpHost()."/storage/".$path."/".$name;

          }else{
            Storage::disk("public")->makeDirectory($path, 0777, true, true);
            $file=Storage::disk("public")->put($path."/".$name,file_get_contents($file));
            return request()->getSchemeAndHttpHost()."/storage/".$path."/".$name;
          } 
      
    }

    public function removeStorage($name){
        $image=explode("/storage",$name);
         if(isset($image[1])){
            if(Storage::disk("public")->delete($image[1])){
                return true;
            }else{
                return false;
            }
         }
        
    }
}
?>