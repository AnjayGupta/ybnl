<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Programmers;
class ProgrammesController extends Controller
{

    public function list(Programmers $program){
        $data=$program->orderBy("id","desc")->simplePaginate(8);
        if(request()->ajax()){
            return $this->Search();
        }
        return view("pages.programmes",compact('data'));  
    }
    public function programmesDetails($slug,Programmers $program){

        $data=$program->with('gallery')->whereSlug($slug)->first();
        return view("pages.programmes_details",compact('data'));
   }



   public function Search(){
     $query=Programmers::query();
     $query->when(request()->search,function($query){
        $query->where(function($query){
                  $query->where("title","LIKE","%".request()->search."%");
        });
     });
     $data=$query->latest()->simplePaginate(10);
      $list=view("pages.ajax.programmes",compact('data'))->render();
      return response()->json(['message'=>"success","result"=>$list]);
   }
}
