<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\JobSeeker;

class JobSeekerController extends Controller
{
    
    public function index(){
        $data = JobSeeker::orderBy("id","desc")->simplePaginate(8);
        if(request()->ajax()){
            return $this->Search();
        }
        return view("pages.job_seekers", compact('data')); 
    }

    public function Search(){
        $query = JobSeeker::query();
        $query->when(request()->search,function($query){
           $query->where(function($query){
                $query->where("title", "LIKE","%".request()->search."%");
           });
        });
        $data = $query->latest()->simplePaginate(10);
        $list = view("pages.ajax.job_seekers", compact('data'))->render();
        return response()->json([
            'message'=>"success",
            "result"=>$list
        ]);
    }

    public function view($slug){
        $data = JobSeeker::whereSlug($slug)->first();
        if($data){
            return view("pages.job_seeker_view", compact('data'));
        }
        else{
            return view('errors.404');
        }
    }

}
