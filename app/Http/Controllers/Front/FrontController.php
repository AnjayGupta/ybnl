<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Events;
use App\Models\Job;
use App\Models\Resource;
use App\Models\Blog;
use App\Models\ContactUs;
use App\Models\Programmers;
use App\Models\TeamMember;
use App\Models\Page;
use App\Models\Banner;
use App\Models\HomepageProfile;

class FrontController extends Controller
{
    public function index(){
      $blog=Blog::latest()->take(3)->get();
      $program=Programmers::latest()->take(4)->get();
      $banners = Banner::where('active', 1)->take(5)->get();
      $profiles = HomepageProfile::where('active', 1)->take(4)->get();
      return view('pages.home',compact('blog', 'program', 'banners', 'profiles'));
      }
     public function abouts_us(){
         $about_page = Page::where('slug', 'what-we-do')->first();
         $advisory_board_members = TeamMember::where('type', 'advisory-board')->get();
         $board_members = TeamMember::where('type', 'board-member')->get();
         return view('pages/abouts_us', compact('advisory_board_members', 'board_members', 'about_page'));
      }
     
      public function jobs(){
       $data = job::orderBy("id","desc")->simplePaginate(10);
       if(request()->ajax()){
         return $this->jobSearch();
       }
         return view('pages/jobs',compact('data'));
      }
      public function resources(){
         $data=Resource::orderBy("id","desc")->simplePaginate(10);
         if(request()->ajax()){
            return $this->resourceSearch();
          }
         return view('pages/resources',compact('data'));
      }
      public function blogs(){
         $data=Blog::orderBy("id","desc")->simplePaginate(10);
         if(request()->ajax()){
            return $this->blogSearch();
          }
         return view('pages/blog',compact('data'));
      }    
      public function events(){
       $data = Events::orderBy("id","desc")->simplePaginate(10);
       if(request()->ajax()){
         return $this->eventSearch();
       }
         return view('pages/events',compact('data'));
      }
      public function contact_us(){
       return view('pages/contact_us');
    }
    public function blog_details($slug){
      $data=Blog::whereSlug($slug)->first();
      if($data){
         return view('pages/blog_details',compact('data'));
      }else{
         return redirect()->back();
      }
       
    }
    public function job_details($slug){
       $slugData = job::where('slug',$slug)->first();
       return view('pages/job_details',compact('slugData'));
    }
    public function event_details($slug){
       $slugData = Events::where('slug',$slug)->first();
       return view('pages.events_details',compact('slugData'));
    }
    

    public function joinCommunity(Request $request){
      return view("pages.join_community");
    }


    


    public function joinCommunityStore(Request $request){
      $request->validate([
         'first_name' => 'required',
         'last_name' => 'required',
         'email' => 'required|email',
         'phone' => 'required',
         'g-recaptcha-response' => 'required|captcha',
     ]);
     $data=$request->only("first_name","last_name","email","phone");
      ContactUs::create($data);
      toastr()->success("","Your data have saved successfully");
      return redirect()->route("join.community");
     
    }

   public function eventSearch(){
      $query=Events::query();
      $query->when(request()->search,function($query){
         $query->where(function($query){
            $query->where("title","LIKE","%".request()->search."%");
        });
      });
      $data=$query->latest()->simplePaginate(10);
       $list=view("pages.ajax.eventList",compact('data'))->render();
       return response()->json(['message'=>"success","result"=>$list]);
      
   }

   public function jobSearch(){
      $query=Job::query();
      $query->when(request()->search,function($query){
         $query->where("title","LIKE","%".request()->search."%");
         $query->orWhere("sub_title","LIKE","%".request()->search."%");
         $query->orWhere("location","LIKE","%".request()->search."%");
         $query->orWhere("slug","LIKE","%".request()->search."%");
      });
      $data=$query->latest()->simplePaginate(10);
       $list=view("pages.ajax.jobList",compact('data'))->render();
       return response()->json(['message'=>"success","result"=>$list]);
   }

   public function blogSearch(){
      $query=Blog::query();
      $query->when(request()->search,function($query){
           $query->where(function($query){
              $query->where("title","LIKE","%".request()->search."%");
              $query->orWhere("slug","LIKE","%".request()->search."%");
           });
      });

      $data=$query->latest()->simplePaginate(10);
      $list=view("pages.ajax.blogList",compact('data'))->render();
      return response()->json(['message'=>"success","result"=>$list]);
   }


   public function resourceSearch(){
      $query=Resource::query();
      $query->when(request()->search,function($query){
            $query->where(function($query){
             $query->where("title","LIKE","%".request()->search."%");
            });
      });
      $data=$query->latest()->simplePaginate(10);
      $list=view("pages.ajax.resourceList",compact('data'))->render();
      return response()->json(['message'=>"success","result"=>$list]);
   }

   public function programmSearch(){

   }

   public function page($slug){
      $page = Page::where('slug', $slug)->first();
      if($page){
         return view('pages.page', compact('page'));
      }
      else{
         return view('errors.404');
      }
   }

   

    
}
