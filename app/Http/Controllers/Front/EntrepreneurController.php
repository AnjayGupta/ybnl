<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Entrepreneur;

class EntrepreneurController extends Controller
{
    
    public function index(){
        $data = Entrepreneur::orderBy("id","desc")->simplePaginate(8);
        if(request()->ajax()){
            return $this->Search();
        }
        return view("pages.entrepreneurs", compact('data')); 
    }

    public function Search(){
        $query = Entrepreneur::query();
        $query->when(request()->search,function($query){
           $query->where(function($query){
                $query->where("title", "LIKE","%".request()->search."%");
           });
        });
        $data = $query->latest()->simplePaginate(10);
        $list = view("pages.ajax.entrepreneurs", compact('data'))->render();
        return response()->json([
            'message'=>"success",
            "result"=>$list
        ]);
    }

    public function view($slug){
        $data = Entrepreneur::whereSlug($slug)->first();
        if($data){
            return view("pages.entrepreneur_view", compact('data'));
        }
        else{
            return view('errors.404');
        }
    }

}
