<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Page;
use Illuminate\Support\Facades\Validator;
use App\Traits\UploadTraits;
use Illuminate\Support\Str;

class PageController extends Controller
{
    use UploadTraits;
    
    public function index(){
        $data = Page::orderBy("id","desc")->simplePaginate(10);
        return view("admin.pages.index",compact('data'));
    }

    public function create(){
        return view("admin.pages.add");
    }

    public function store(Request $request){
        $validate=Validator::make($request->all(), Page::rule());
        if($validate->fails()){
          $messages = $validate->messages()->first();
          toastr()->error('',$messages);
          return redirect()->back()->withInput($request->all());
        }else{

            $check = Page::where('slug', Str::slug($request->title))->first();
            if($check){
                toastr()->error('', 'Page already exists. Try with different title.');
                return redirect()->back()->withInput($request->all());
            }
            Page::create([
                "slug" => Str::slug($request->title),
                "title" => $request->title,
                "short_description" => $request->short_description,
                "long_description" => $request->long_description,
                "content" => $request->content,
                "cover_color" => $request->cover_color,
                "cover_image" => $request->hasFile("cover_image") ? $this->uploadStorage($request->file("cover_image"), "pages") : null
            ]);
            toastr()->success('', "Page added successfully");
            return redirect()->back();
  
        }
    }

    public function edit($id){
        $id = decode($id);
        $data = Page::find($id);
        return view("admin.pages.edit", compact('data'));
    }

    public function update(Request $request){
        $rule = Page::rule();
        $rule['id'] = "required|exists:pages,id";
        unset($rule['cover_image']);
        $validate=Validator::make($request->all(),$rule);

        if($validate->fails()){
            $messages = $validate->messages()->first();
            toastr()->error('',$messages);
            return redirect()->back()->withInput($request->all());
        }
        else{
            $page = Page::find($request->id);
            if($request->hasFile("cover_image")){
                $this->removeStorage($page->cover_image);
                $page->cover_image = $this->uploadStorage($request->file("cover_image"), "pages");
            }
            $page->title = $request->title;
            $page->short_description = $request->short_description;
            $page->long_description = $request->long_description;
            $page->content = $request->content;
            $page->cover_color = $request->cover_color;
            $page->save();

            toastr()->success('',"Page updated successfully");
            return redirect()->route("admin.page.index");
  
        }
    }

    public function delete($id){
        $id = decode($id);
        $page = Page::find($id);
        if($page){
            $this->removeStorage($page->cover_image);
            $page->delete();
            toastr()->success('',"Page deleted successfully");
            return redirect()->back();
        }else{
            toastr()->error('',"Page not found ");
            return redirect()->back();
        }
    }

}
