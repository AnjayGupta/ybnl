<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use App\Traits\UploadTraits;
class BlogController extends Controller
{

    use UploadTraits;
    public function index(){
        return view("admin.blog.add");
    }

    public function store(Request $request){
      $validate=Validator::make($request->all(),Blog::rule());
      if($validate->fails()){
        $messages = $validate->messages()->first();
        toastr()->error('',$messages);
        return redirect()->back()->withInput($request->all());
      }else{
            Blog::create([
                "title"=>$request->title,
                "image"=>$this->uploadStorage($request->file("image"),"blog"),
                "description"=>$request->description,
                "slug"=>Str::slug($request->title),
            ]);
            toastr()->success('',"Blog created successfully");
        return redirect()->back();

      }
    }

    public function list(Blog $blog){
    $data=$blog->orderBy("id","desc")->simplePaginate(10);
    return view("admin.blog.list",compact('data'));
    }

    public function edit($id){
      $id=decode($id);
      $data=Blog::find($id);
      return view("admin.blog.edit",compact('data'));
    }
    public function update(Request $request){
        $rule=Blog::rule();
        $rule['id']="required|exists:blogs,id";
        unset($rule['image']);
        $validate=Validator::make($request->all(),$rule);
        if($validate->fails()){
          $messages = $validate->messages()->first();
          toastr()->error('',$messages);
          return redirect()->back()->withInput($request->all());
        }else{
            $blog=Blog::find($request->id);
            if($request->hasFile("image")){
                $this->removeStorage($blog->image);
            $blog->image=$this->uploadStorage($request->file("image"),"blog");
            }
            $blog->title=$request->title;
            $blog->description=$request->description;
            $blog->slug=Str::slug($request->title);
            $blog->save();
           toastr()->success('',"Blog updated successfully");
          return redirect()->route("admin.blog.list");
  
        }
    }

    public function delete($id){
    $id=decode($id);
    $blog=Blog::find($id);
    if($blog){
        $this->removeStorage($blog->image);
        $blog->delete();
        toastr()->success('',"Blog deleted successfully");
        return redirect()->back();
    }else{
        toastr()->error('',"Blog Not found ");
        return redirect()->back();
    }
    }
}
