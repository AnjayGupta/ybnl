<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Programmers;
use App\Models\ProgrammerImage;
use App\Traits\UploadTraits;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB, Exception;

class ProgrammersController extends Controller
{
     use UploadTraits;
    public function index(){
        $data = Programmers::orderBy("id","desc")->simplePaginate(10);
        return view('admin.programmers',compact('data'));
    }

    public function add_programmer(){

        return view('admin.add_programmer');
    }
    public function store(Request $request){

        try{
            $rules = array(
                'title'=> 'required',
                'image' => 'required|mimes:jpg,png,svg,jpeg',
                'images.*' => 'mimes:jpg,png,svg,jpeg',
                'description' => 'required',
                "docs"=>"required|max:5020|mimes:pdf",
            );
    
            $validator = Validator::make($request->all(),$rules);
            if ($validator->fails())
            {
            $messages = $validator->messages();
            toastr()->error("",$messages->first());
            return redirect()->back();
            }
            
            $image = $this->uploadStorage($request->file("image"),"programmes");
            $docs=$this->uploadStorage($request->file("docs"),"programmes");
            DB::beginTransaction();
            $inserted = Programmers::create(['title'=>$request->title,'desc'=>$request->description,
            'image'=>$image,"slug"=>Str::slug($request->title),
            "docs"=>$docs,"eligibility_criteria"=>$request->eligibility_criteria,"process"=>$request->process
            ]);
            if ($inserted) {
                foreach($request->file('images') as $i){
                    $image = $this->uploadStorage($i,"programmes_gallery");
                    $data[] = array('programmers_id'=>$inserted->id,'path'=>$image,'created_at'=>now(),'updated_at'=>now());
                }
                ProgrammerImage::insert($data);
                toastr()->success("",'Programmer added successfully!');
                DB::commit();
                return redirect()->back();
            }
        } catch (Exception $e) {
            DB::rollBack();
            toastr()->error($e->getMessage());
            return redirect()->back();
        }
    }
    public function edit(Request $request){
        $editData = Programmers::with('gallery')->find($request->id);
        return view('admin.edit_programer',compact('editData'));
    }
    public function update(Request $request){
        try{
            $rules = array(
                'title'=> 'required',
                'description' => 'required',
                'image' => 'mimes:jpg,png,svg,jpeg',
                'images.*' => 'mimes:jpg,png,svg,jpeg',
            );
    
            $validator = Validator::make($request->all(),$rules);
            if ($validator->fails())
            {
                $messages = $validator->messages();
                toastr()->warning($messages->first());
                return redirect()->back();
            }
            $data =  Programmers::find($request->id);
            if($data){
                DB::beginTransaction();
                if($request->hasFile("image")){
                    $this->removeStorage($data->image);
                    $data->image=$this->uploadStorage($request->file("image"),"programmes");
                }
                if($request->hasFile("docs")){
                    $this->removeStorage($data->docs);
                    $data->docs=$this->uploadStorage($request->file("docs"),"programmes");
                }
            
                $data->title=$request->title;
                $data->desc=$request->description;
                $data->eligibility_criteria=$request->eligibility_criteria;
                $data->process=$request->process;
                $data->slug=Str::slug($request->title);
                if ($data->save()) {
                    if($request->images){
                        foreach($request->file('images') as $i){
                            $image = $this->uploadStorage($i,"programmes_gallery");
                            $gallery_data[] = array('programmers_id'=>$request->id,'path'=>$image,'created_at'=>now(),'updated_at'=>now());
                        }
                        ProgrammerImage::insert($gallery_data);
                    }
                    
                    toastr()->success('','Details Updated successfully!');
                    DB::commit();
                    return redirect()->route('admin.programmers');
                }
            }else{
                toastr()->error('','Whoops! something is wrong try again');
                return redirect()->route('admin.programmers');
            }
        } catch (Exception $e) {
            DB::rollBack();
            toastr()->error($e->getMessage());
            return redirect()->back();
        }
    }
    public function delete(request $request){
        $programmes = Programmers::find($request->id);
        if ($programmes) {
            $this->removeStorage($programmes->image);
            $this->removeStorage($programmes->docs);
            $programmes->delete();
            toastr()->success('','Programmers Deleted successfully!');
            return redirect()->route('admin.programmers');
        } 
    }    

    public function deleteGalleryImage($id){
        $image = ProgrammerImage::find($id);
        if ($image) {
            $this->removeStorage($image->path);
            $image->delete();
            toastr()->success('','Gallery Image Deleted successfully!');
            return redirect()->back();
        }
        else{
            toastr()->warning('','Image not found!');
            return redirect()->back();
        }
    }
}
