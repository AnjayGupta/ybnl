<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TeamMember;
use Illuminate\Support\Facades\Validator;
use App\Traits\UploadTraits;

class TeamMemberController extends Controller
{
    use UploadTraits;

    public function index(){
        $data = TeamMember::orderBy("id","desc")->simplePaginate(10);
        return view("admin.team_member.index",compact('data'));
    }

    public function create(){
        return view("admin.team_member.add");
    }

    public function store(Request $request){
        $validate=Validator::make($request->all(), TeamMember::rule());
        if($validate->fails()){
          $messages = $validate->messages()->first();
          toastr()->error('',$messages);
          return redirect()->back()->withInput($request->all());
        }else{
              TeamMember::create([
                  "name" => $request->name,
                  "designation" => $request->designation,
                  "type" => $request->type,
                  "image" => $request->type=="advisory-board" ? $this->uploadStorage($request->file("image"), "team_member") : null
              ]);
              toastr()->success('', "Team member added successfully");
          return redirect()->back();
  
        }
    }

    public function edit($id){
        $id = decode($id);
        $data = TeamMember::find($id);
        return view("admin.team_member.edit", compact('data'));
    }

    public function update(Request $request){
        $rule = TeamMember::rule();
        $rule['id'] = "required|exists:team_members,id";
        unset($rule['image']);
        $validate=Validator::make($request->all(),$rule);

        if($validate->fails()){
            $messages = $validate->messages()->first();
            toastr()->error('',$messages);
            return redirect()->back()->withInput($request->all());
        }
        else{
            $team_member = TeamMember::find($request->id);
            if($request->hasFile("image")){
                $this->removeStorage($team_member->image);
                $team_member->image = $this->uploadStorage($request->file("image"), "team_member");
            }
            $team_member->name = $request->name;
            $team_member->designation = $request->designation;
            $team_member->type = $request->type;
            $team_member->save();

            toastr()->success('',"Team member updated successfully");
            return redirect()->route("admin.team_member.index");
  
        }
    }

    public function delete($id){
        $id = decode($id);
        $team_member = TeamMember::find($id);
        if($team_member){
            $this->removeStorage($team_member->image);
            $team_member->delete();
            toastr()->success('',"Team member deleted successfully");
            return redirect()->back();
        }else{
            toastr()->error('',"Team member not found ");
            return redirect()->back();
        }
    }
}
