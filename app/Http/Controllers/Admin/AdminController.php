<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Job;
use App\Models\Events;
use App\Models\AppliedJob;
use App\Models\Programmers;
use App\Models\User;
use App\Traits\UploadTraits;
use Auth;
class AdminController extends Controller
{
    use UploadTraits;
    public function index(){
        $job=Job::count();
        $event=Events::count();
        $applied_job=AppliedJob::count();
        $program=Programmers::count();
        return view('admin.dashboard',compact('job','event','applied_job','program'));
    }
    public function programmers(){
       
        return view('admin.programmers');
    }

    public function updateProfile(Request $request){
        $user=Auth::user();
        if($request->hasFile("profile")){
         if($user->profile!=""){
            $this->removeStorage($user->profile);
         }
         $user->profile=$this->uploadStorage($request->file("profile"),"user");
        }
        $user->fill($request->only('name','email','address','phone'));
        $user->save();
        return response()->json(['message'=>"Profile updated successfully",'status'=>1],200);
    }
}
