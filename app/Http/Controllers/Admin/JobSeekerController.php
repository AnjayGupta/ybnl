<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\JobSeeker;
use App\Traits\UploadTraits;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class JobSeekerController extends Controller
{
    use UploadTraits;
    
    public function index(){
        $data = JobSeeker::orderBy("id","desc")->simplePaginate(10);
        return view('admin.job_seeker.index', compact('data'));
    }

    public function create(){
        return view('admin.job_seeker.create');
    }

    public function store(Request $request){
        
        try{
            $rules = array(
                'title' => 'required',
                'description' => 'required',
                'image' => 'required|mimes:jpg,png,svg,jpeg',
                "document" => "required|max:5020|mimes:pdf"
            );
    
            $validator = Validator::make($request->all(),$rules);
    
            if ($validator->fails()){
                $messages = $validator->messages();
                toastr()->error("", $messages->first());
                return redirect()->back();
            }
    
            $check = JobSeeker::where('slug', Str::slug($request->title))->first();
            if($check){
                toastr()->error('', 'Job seeker already exists. Try with different title.');
                return redirect()->back()->withInput($request->all());
            }
    
            $image = $this->uploadStorage($request->file("image"), "job_seekers");
            $document = $this->uploadStorage($request->file("document"), "job_seekers");
    
            JobSeeker::create([
                "slug" => Str::slug($request->title),
                'title' => $request->title,
                'description' => $request->description,
                'image' => $image,
                "document" => $document
            ]);
    
            toastr()->success("", 'Job seeker added successfully!');
            return redirect()->back();
        }
        catch (\Exception $e) {
            toastr()->error("", $e->getMessage());
            return redirect()->back();
        }

    }

    public function edit(Request $request){
        $data = JobSeeker::find($request->id);
        return view('admin.job_seeker.edit', compact('data'));
    }

    public function update(Request $request){
        
        try{
            $rules = array(
                'id' => 'required|exists:job_seekers,id',
                'title' => 'required',
                'description' => 'required'
            );
    
            $validator = Validator::make($request->all(),$rules);
    
            if ($validator->fails()){
                $messages = $validator->messages();
                toastr()->error("",$messages->first());
                return redirect()->back();
            }
    
            $job_seeker = JobSeeker::find($request->id);
            if(!$job_seeker){
                toastr()->error('', 'Job seeker not found.');
                return redirect()->back()->withInput($request->all());
            }

            if($request->hasFile("image")){
                $this->removeStorage($job_seeker->image);
                $job_seeker->image=$this->uploadStorage($request->file("image"), "job_seekers");
            }
            if($request->hasFile("document")){
                $this->removeStorage($job_seeker->docs);
                $job_seeker->document=$this->uploadStorage($request->file("document"), "job_seekers");
            }

            $job_seeker->title = $request->title;
            $job_seeker->description = $request->description;
    
            if ($job_seeker->save()) {
                toastr()->success('', 'Details Updated successfully!');
                return redirect()->route('admin.job_seeker.index');
            }
            else{
                toastr()->error('', 'Whoops! something is wrong try again');
                return redirect()->back();
            }
        }
        catch (\Exception $e) {
            toastr()->error("", $e->getMessage());
            return redirect()->back();
        }

    }

    public function delete($id){
        $job_seeker = JobSeeker::find($id);
        if($job_seeker){
            $this->removeStorage($job_seeker->image);
            $this->removeStorage($job_seeker->document);
            $job_seeker->delete();
            toastr()->success('',"Job seeker deleted successfully");
            return redirect()->back();
        }else{
            toastr()->error('',"Job seeker not found ");
            return redirect()->back();
        }
    }

}
