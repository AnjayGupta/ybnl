<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ContactUs;
class CommunityController extends Controller
{
    public function index(){
        $data=ContactUs::latest()->simplePaginate(10);
       return view("admin.community.index",compact('data'));
    }

    public function delete($id){
    $id=decode($id);
    $data=ContactUs::findOrFail($id);
    if($data){
        $data->delete();
        toastr()->success('',"Community deleted successfully");
        return redirect()->route("admin.community.list");
    }else{
        toastr()->error('',"Data not found");
        return redirect()->route("admin.community.list");
    }
    }
}
