<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Banner;
use App\Models\HomepageProfile;
use App\Traits\UploadTraits;
use Illuminate\Support\Facades\Validator;

class HomepageSettingsController extends Controller
{
    use UploadTraits;

    public function banners(){
        $data = Banner::orderBy("id","desc")->simplePaginate(10);
        return view('admin.homepage_settings.banners.index', compact('data'));
    }

    public function bannerCreate(){
        return view('admin.homepage_settings.banners.add');
    }

    public function bannerStore(Request $request){
        
        try{
            $rules = array(
                'title' => 'required',
                'short_description' => 'required',
                'long_description' => 'required',
                'cover_image' => 'required|mimes:jpg,png,svg,jpeg'
            );
    
            $validator = Validator::make($request->all(),$rules);
    
            if ($validator->fails()){
                $messages = $validator->messages();
                toastr()->error("",$messages->first());
                return redirect()->back();
            }
    
            $image = $this->uploadStorage($request->file("cover_image"), "banners");
    
            Banner::create([
                'title' => $request->title,
                'short_description' => $request->short_description,
                'long_description' => $request->long_description,
                'cover_image' => $image,
                'active' => true
            ]);
    
            toastr()->success("", 'Banner added successfully!');
            return redirect()->back();
        }
        catch (\Exception $e) {
            toastr()->error("", $e->getMessage());
            return redirect()->back();
        }

    }

    public function bannerEdit(Request $request){
        $data = Banner::find($request->id);
        return view('admin.homepage_settings.banners.edit', compact('data'));
    }

    public function bannerUpdate(Request $request){
        
        try{
            $rules = array(
                'title' => 'required',
                'short_description' => 'required',
                'long_description' => 'required',
            );
    
            $validator = Validator::make($request->all(),$rules);
    
            if ($validator->fails()){
                $messages = $validator->messages();
                toastr()->error("",$messages->first());
                return redirect()->back();
            }
    
            $banner = Banner::find($request->id);
            if(!$banner){
                toastr()->error('', 'Banner not found.');
                return redirect()->back()->withInput($request->all());
            }

            if($request->hasFile("cover_image")){
                $this->removeStorage($banner->cover_image);
                $banner->cover_image = $this->uploadStorage($request->file("cover_image"), "banners");
            }

            $banner->title = $request->title;
            $banner->short_description = $request->short_description;
            $banner->long_description = $request->long_description;
    
            if ($banner->save()) {
                toastr()->success('','Details Updated successfully!');
                return redirect()->route('admin.banners');
            }
            else{
                toastr()->error('','Whoops! something is wrong try again');
                return redirect()->back();
            }
        }
        catch (\Exception $e) {
            toastr()->error("", $e->getMessage());
            return redirect()->back();
        }

    }

    public function bannerDelete($id){
        $banner = Banner::find($id);
        if($banner){
            $this->removeStorage($banner->cover_image);
            $banner->delete();
            toastr()->success('',"Banner deleted successfully");
            return redirect()->back();
        }else{
            toastr()->error('',"Banner not found ");
            return redirect()->back();
        }
    }

    


    public function profiles(){
        $data = HomepageProfile::orderBy("id","desc")->simplePaginate(10);
        return view('admin.homepage_settings.profiles.index', compact('data'));
    }

    public function profileCreate(){
        return view('admin.homepage_settings.profiles.add');
    }

    public function profileStore(Request $request){
        
        try{
            $rules = array(
                'title' => 'required',
                'description' => 'required',
                'overlay_color' => 'required',
                'image' => 'required|mimes:jpg,png,svg,jpeg'
            );
    
            $validator = Validator::make($request->all(),$rules);
    
            if ($validator->fails()){
                $messages = $validator->messages();
                toastr()->error("",$messages->first());
                return redirect()->back();
            }
    
            $image = $this->uploadStorage($request->file("image"), "homepage_profiles");
    
            HomepageProfile::create([
                'title' => $request->title,
                'description' => $request->description,
                'overlay_color' => $request->overlay_color,
                'image' => $image,
                'active' => true
            ]);
    
            toastr()->success("", 'Profile added successfully!');
            return redirect()->back();
        }
        catch (\Exception $e) {
            toastr()->error("", $e->getMessage());
            return redirect()->back();
        }

    }

    public function profileEdit(Request $request){
        $data = HomepageProfile::find($request->id);
        return view('admin.homepage_settings.profiles.edit', compact('data'));
    }

    public function profileUpdate(Request $request){
        
        try{
            $rules = array(
                'title' => 'required',
                'description' => 'required',
                'overlay_color' => 'required',
            );
    
            $validator = Validator::make($request->all(),$rules);
    
            if ($validator->fails()){
                $messages = $validator->messages();
                toastr()->error("",$messages->first());
                return redirect()->back();
            }
    
            $profile = HomepageProfile::find($request->id);
            if(!$profile){
                toastr()->error('', 'Profile not found.');
                return redirect()->back()->withInput($request->all());
            }

            if($request->hasFile("image")){
                $this->removeStorage($profile->image);
                $profile->image = $this->uploadStorage($request->file("image"), "homepage_profiles");
            }

            $profile->title = $request->title;
            $profile->description = $request->description;
            $profile->overlay_color = $request->overlay_color;
    
            if ($profile->save()) {
                toastr()->success('','Details Updated successfully!');
                return redirect()->route('admin.profiles');
            }
            else{
                toastr()->error('','Whoops! something is wrong try again');
                return redirect()->back();
            }
        }
        catch (\Exception $e) {
            toastr()->error("", $e->getMessage());
            return redirect()->back();
        }

    }

    public function profileDelete($id){
        $profile = HomepageProfile::find($id);
        if($profile){
            $this->removeStorage($profile->image);
            $profile->delete();
            toastr()->success('',"Profile deleted successfully");
            return redirect()->back();
        }else{
            toastr()->error('',"Profile not found ");
            return redirect()->back();
        }
    }

}
