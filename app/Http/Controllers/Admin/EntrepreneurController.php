<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Entrepreneur;
use App\Traits\UploadTraits;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class EntrepreneurController extends Controller
{
    use UploadTraits;

    public function index(){
        $data = Entrepreneur::orderBy("id","desc")->simplePaginate(10);
        return view('admin.entrepreneur.index', compact('data'));
    }

    public function create(){
        return view('admin.entrepreneur.add');
    }

    public function store(Request $request){
        
        try{
            $rules = array(
                'title' => 'required',
                'description' => 'required',
                'image' => 'required|mimes:jpg,png,svg,jpeg',
                "document" => "required|max:5020|mimes:pdf"
            );
    
            $validator = Validator::make($request->all(),$rules);
    
            if ($validator->fails()){
                $messages = $validator->messages();
                toastr()->error("",$messages->first());
                return redirect()->back();
            }
    
            $check = Entrepreneur::where('slug', Str::slug($request->title))->first();
            if($check){
                toastr()->error('', 'Entrepreneur already exists. Try with different title.');
                return redirect()->back()->withInput($request->all());
            }
    
            $image = $this->uploadStorage($request->file("image"), "entrepreneurs");
            $document = $this->uploadStorage($request->file("document"), "entrepreneurs");
    
            Entrepreneur::create([
                "slug" => Str::slug($request->title),
                'title' => $request->title,
                'description' => $request->description,
                'image' => $image,
                "document" => $document
            ]);
    
            toastr()->success("", 'Entrepreneur added successfully!');
            return redirect()->back();
        }
        catch (\Exception $e) {
            toastr()->error("", $e->getMessage());
            return redirect()->back();
        }

    }

    public function edit(Request $request){
        $data = Entrepreneur::find($request->id);
        return view('admin.entrepreneur.edit', compact('data'));
    }

    public function update(Request $request){
        
        try{
            $rules = array(
                'id' => 'required|exists:entrepreneurs,id',
                'title' => 'required',
                'description' => 'required'
            );
    
            $validator = Validator::make($request->all(),$rules);
    
            if ($validator->fails()){
                $messages = $validator->messages();
                toastr()->error("",$messages->first());
                return redirect()->back();
            }
    
            $entrepreneur = Entrepreneur::find($request->id);
            if(!$entrepreneur){
                toastr()->error('', 'Entrepreneur not found.');
                return redirect()->back()->withInput($request->all());
            }

            if($request->hasFile("image")){
                $this->removeStorage($entrepreneur->image);
                $entrepreneur->image=$this->uploadStorage($request->file("image"), "entrepreneurs");
            }
            if($request->hasFile("document")){
                $this->removeStorage($entrepreneur->docs);
                $entrepreneur->document=$this->uploadStorage($request->file("document"),"entrepreneurs");
            }

            $entrepreneur->title = $request->title;
            $entrepreneur->description = $request->description;
    
            if ($entrepreneur->save()) {
                toastr()->success('','Details Updated successfully!');
                return redirect()->route('admin.entrepreneur.index');
            }
            else{
                toastr()->error('','Whoops! something is wrong try again');
                return redirect()->back();
            }
        }
        catch (\Exception $e) {
            toastr()->error("", $e->getMessage());
            return redirect()->back();
        }

    }

    public function delete($id){
        $entrepreneur = Entrepreneur::find($id);
        if($entrepreneur){
            $this->removeStorage($entrepreneur->image);
            $this->removeStorage($entrepreneur->document);
            $entrepreneur->delete();
            toastr()->success('',"Entrepreneur deleted successfully");
            return redirect()->back();
        }else{
            toastr()->error('',"Entrepreneur not found ");
            return redirect()->back();
        }
    }
}
