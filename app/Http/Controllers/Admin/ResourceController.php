<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Resource;
use App\Traits\UploadTraits;
class ResourceController extends Controller
{
    use UploadTraits;
    public function create(Request $request){
       return view("admin.resource.index");
    }

    public function store(Request $request,Resource $resource){
        try{
            $request->validate(Resource::$rule,Resource::$message);
         $input=$request->all();
         $input['docs']=$this->uploadStorage($request->file("docs"),"resource");
         $resource->create($input);
         toastr()->success("","Resource Added Successfully");
            return redirect()->back();
        }catch(\Exception $e){
            toastr()->error("",$e->getMessage());
            return redirect()->back();
        }
        
       

    }

    public function list(Resource $resource){
        try{
            $data=$resource->orderBy("id","desc")->simplePaginate(10);
            return view("admin.resource.list",compact('data'));

        }catch(\Exception $e){
            toastr()->error("",$e->getMessage());
            return redirect()->back();
        }

    }

    public function edit(Request $request,Resource $resource){
       $data=$resource->find($request->id);
       return view("admin.resource.edit",compact('data'));
    }

    public function update(Request $request,Resource $resource){
        $request->validate($resource->updateRule());
         $data=$resource->find(decode($request->id));
         if($request->hasFile("docs")){
            $this->removeStorage($data->docs);
            $data->docs=$this->uploadStorage($request->file("docs"),"resource");
         }
         $data->title=$request->title;
         $data->desc=$request->desc;
         $data->save();
         toastr()->success("","Resource Updated Successfully");
         return redirect()->route("admin.resource.list");
        
    }


    public function delete(Request $request,Resource $resource){
        $data=$resource->find($request->id);
        if($data){
            $this->removeStorage($data->docs);
            $data->delete();
            toastr()->success("","Resource Deleted Successfully");
            return redirect()->back();
        }else{
            toastr()->error("","Whoops! something is wrong try again");
            return redirect()->back();
        }

    }
}
