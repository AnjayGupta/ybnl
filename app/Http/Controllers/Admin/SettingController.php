<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AdminSetting;
use Str;
class SettingController extends Controller
{
    public function index(){
        $setting=AdminSetting::first();
        return view("admin.settings.index",compact('setting'));
    }
    public function update(Request $request){
        
        $data=$request->only("email","phone","facebook_link","twitter_link","youtube_link","linkedin","address","timing");
         if($request->hasFile("app_icon")){
            $filename=Str::random(20).".".$request->file("app_icon")->getClientOriginalName();
            $request->file("app_icon")->move("icon/",$filename);
            $filename=url('icon')."/".$filename;
            $data['app_icon']=$filename;
        }
        $create=AdminSetting::updateOrCreate(['id'=>1],$data);
        if($create){
            toastr()->success('',"Setting Updated Successfully");
            return redirect()->back();
        }else{
            toastr()->error('',"Whoops! something is wrong try again");
            return redirect()->back();
        }

    }
    
}
