<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AppliedJob;
use Illuminate\Support\Str;
use App\Jobs\SendMailAppliedJob;
use App\Models\Job;
class AppliedJobController extends Controller
{
    public function applied(Request $request){
    
        $request->validate(AppliedJob::$rule,AppliedJob::$message);
        $input=$request->all();
        $input['job_id']=decode($request->job_id);
       $input['desc']=$request->description;
        $resume=$request->file("resume");
         $filename=Str::random(20).".".$resume->extension();
         $resume->move("resume",$filename);
         $filename=url('resume')."/".$filename;
          $input['resume']=$filename; 
          $applied_data= $input;
        $create= AppliedJob::create($input);
        $data=Job::find($input['job_id'])->select("title");
          $input['job_title']=$data;
        $job = (new SendMailAppliedJob($applied_data));
        dispatch($job);

        if($create){
            toastr()->success('','Job Applied SUccessfully');
            return redirect()->back();
        }else{
            toastr()->error('','Whoops! something is wrong try again');
            return redirect()->back();
        }

    }

    public function list(Request $request){
        $data=AppliedJob::join("job",function($join){
            $join->on("job.id","=","applied_jobs.job_id");
        })->select("name","email","applied_jobs.desc","resume","subject","applied_jobs.id","job.title")->orderBy("id","desc")->paginate(10);
     
        return view("admin.jobs.applied_job",compact('data'));
    }

    public function delete(Request $request){
        $id=decode($request->id);
        $data=AppliedJob::find($id);
        if($data){
            $data->delete();
            toastr()->success('','Applied Job Deleted Successfully');
            return redirect()->back();
        }else{
            toastr()->error('','Whoops! something is wrong try again');
            return redirect()->back();
        }
    }
}
