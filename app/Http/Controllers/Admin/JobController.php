<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Job;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Rules\NotUrl;
class JobController extends Controller
{
    public function index(){
        $data = Job::orderBy("id","desc")->simplePaginate(10);
       
        return view('admin.jobs.jobs',compact('data'));
    }

    public function add(){

        return view('admin.jobs.add_job');
    }
    public function store(Request $request){

        $rules = array(
            'title'=> 'required',
            'sub_title' => 'required',
            'location' => 'required',
            'description' => 'required',
        );
        if(filter_var($request->link,FILTER_VALIDATE_EMAIL)){
            $rules['link']="required|email";
        }else{
            $rules['link']=['required', 'url'];
        }

        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails())
        {
        $messages = $validator->messages();
        toastr()->error('',$messages->first());
        return redirect()->back()->withInput($request->all());
        }
        
       $inserted = Job::insert([
        'title'=>$request->title,
        'sub_title'=>$request->sub_title,
        'location'=>$request->location,
        'desc'=>$request->description,
        "link"=>$request->link,
        'slug'=>Str::slug($request->title)
    ]);
        if ($inserted) {
            toastr()->success('','Job Added successfully!');
            return redirect()->route('job.index');
        } 
    }
    public function edit(Request $request){
        $editData = Job::find($request->id);
        return view('admin.jobs.edit_job',compact('editData'));
    }
    public function update(Request $request){

       $rules = array(
        'title'=> 'required',
        'sub_title' => 'required',
        'location' => 'required',
        'description' => 'required',
        
    );

    $validator = Validator::make($request->all(),$rules);
    if ($validator->fails())
    {
    $messages = $validator->messages();
    toastr()->warning('All fields are required !');
    return redirect()->route('job.index');
    }

   $data = Job::find($request->id);
   $details=array('title'=>$request->title,
   'sub_title'=>$request->sub_title,
   'location'=>$request->location,
   "link"=>$request->link,
   'desc'=>$request->description,
   'slug'=>Str::slug($request->title));
  $data->fill( $details);
    if ($data->save()) {
        toastr()->success('','Details Updated successfully!');
        return redirect()->route('job.index');
    } 
    }
    public function delete(request $request){
        $delete = Job::destroy($request->id);
        if ($delete) {
            toastr()->success('Deleted successfully!');
            return redirect()->route('job.index');
        } 
    }  
}
