<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Events;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Traits\UploadTraits;
class EventsController extends Controller
{
    use UploadTraits;
    public function index(){
        $data = Events::orderBy("id","desc")->simplePaginate(10);
        return view('admin.events.event',compact('data'));
    }

    public function add(){

        return view('admin.events.add_event');
    }
    public function store(Request $request){
        
       try{
        $rules = array(
            'title'=> 'required',
            'image' => 'required',
            'description' => 'required',
        );

        $validator = Validator::make($request->all(),$rules);
        if ($validator->fails())
        {
        $messages = $validator->messages();
        toastr()->warning('',$messages->first());
        return redirect()->back();
        }
        $path=$this->uploadStorage($request->file("image"),"events");
        $inserted = Events::insert(['title'=>$request->title,'desc'=>$request->description,'image'=>$path,'slug'=>Str::slug($request->title)]);
        if ($inserted) {
            toastr()->success('','Programmer added successfully!');
            return redirect()->route('admin.event.index');
        } 

       }catch(\Exception $e){
        toastr()->error('',$e->getMessage());
        return redirect()->route('admin.event.index');
       }
    }
    public function edit(Request $request){
        $editData = Events::find($request->id);
        return view('admin.events.edit_event',compact('editData'));
    }
    public function update(Request $request){
        
       try{
        $event =  Events::find($request->id);
       
       $rules = array(
        'title'=> 'required',
        'description' => 'required',
    );

    $validator = Validator::make($request->all(),$rules);
    if ($validator->fails())
    {
    $messages = $validator->messages();
    toastr()->warning('',$messages->first());
    return redirect()->back();
    }

    if(!empty($request->image)){
        $this->removeStorage($event->image);
        $event->image=$this->uploadStorage($request->file("image"),"events");
    }
     $event->fill(['title'=>$request->title,'desc'=>$request->description,'slug'=>Str::slug($request->title)]);
     $updated=$event->save();
    if ($updated) {
        toastr()->success('','Details Updated successfully!');
        return redirect()->route('admin.event.index');
    } 

       }catch(\Exception $e){
        toastr()->error('',$e->getMessage());
        return redirect()->route('admin.event.index');
       }
    }
    public function delete(request $request){
        $event = Events::find($request->id);
        if ($event) {
            $this->removeStorage($event->image);
            $event->delete();
            toastr()->success('','Event Deleted successfully!');
            return redirect()->route('admin.event.index');
        } else{
            toastr()->error('','Event Not found !');
            return redirect()->route('admin.event.index');
        }
    } 
}
