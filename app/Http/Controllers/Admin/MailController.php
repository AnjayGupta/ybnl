<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Mail;
class MailController extends Controller
{
    public function create(Request $request){
        $data=Mail::orderBy("id","desc")->simplePaginate(10);
        return view("admin.mails.index",compact('data'));
    }

    public function store(Request $request){
        $request->validate(Mail::$rule,Mail::$message);
        $data=$request->only("email","name");
        $create=Mail::create($data);
        if($create){
            toastr()->success('','Mail added successfully!');
            return redirect()->back();
        }else{
            toastr()->error('','Whoops! something is wrong try again');
            return redirect()->back();
        }
    }


    public function delete(Request $request){
        $id=decode($request->id);
        $data=Mail::find($id);
        if($data){
            $data->delete();
            toastr()->success('','Mail Deleted successfully!');
            return redirect()->back();
        }else{
            toastr()->error('','Whoops! something is wrong try again');
            return redirect()->back();
        }
    }
}
